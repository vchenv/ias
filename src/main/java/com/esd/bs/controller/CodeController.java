package com.esd.bs.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.esd.core.JuBao;
import com.esd.core.WebclientFormSubmit;

@Controller
public class CodeController {
	WebclientFormSubmit wfs = new WebclientFormSubmit();
	/**
	 * 书记、区长信箱进入页面获取验证码
	 * cc 20171024
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/xxUrl" ,  method = RequestMethod.GET)
	@ResponseBody
	public void xxUrl(HttpServletRequest request){
		String url = null;
		String mark = request.getParameter("mark");
		System.out.println("mark==>"+mark);
		if(mark.equals("sjxx")){
			url = "http://www.bjmtg.gov.cn/hdpt/sjxx/";
		}
		if(mark.equals("qzxx")){
			url = "http://www.bjmtg.gov.cn/hdpt/qzxx/";
		}
		wfs.download(url);
	}
	
	/**
	 * 书记、区长信箱提交
	 * cc 20171024
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/xxsubmit" ,  method = RequestMethod.POST)
	@ResponseBody
	public int xxsubmit(HttpServletRequest request,HttpSession session){
		Map<String, String> map = new HashMap<String, String>();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String tel = request.getParameter("tel");
		String title = request.getParameter("title");
		String content= request.getParameter("content");
		String certicode= request.getParameter("certicode");
		System.out.println("certicode====>"+certicode);
		String randomCode = session.getAttribute("randomCode").toString();
		System.out.println("randomCode===>"+randomCode);
		map.put("name",name );
		map.put("email",email );
		map.put("tel",tel );
		map.put("title",title );
		map.put("content",content );
		map.put("certicode",certicode );
		return wfs.download(map,randomCode);
	}
	
	/**
	 * 在线求助进入页面获取验证码
	 * cc  20171024
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "zxqz" , method = RequestMethod.POST)
	@ResponseBody
	public void zxqz(HttpServletRequest request){
		String url = request.getParameter("url");
		wfs.zxqzDownload(url);
	}
	/**
	 * 在线求助提交
	 * cc  20171024
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "zxqzSubmit" , method = RequestMethod.POST)
	@ResponseBody
	public int zxqzSubmit(HttpServletRequest request ,HttpSession session){
		Map<String, String> map = new HashMap<String, String>();
		String isNiMing = request.getParameter("isNiMing");
		String Name = request.getParameter("Name");
		String Sex = request.getParameter("Sex");
		String Phone = request.getParameter("Phone");
		String Address = request.getParameter("Address");
		String Con= request.getParameter("Con");
		String certicode= request.getParameter("certicode");
		String randomCode = session.getAttribute("randomCode").toString();
		map.put("isNiMing",isNiMing );
		map.put("Name",Name );
		map.put("Sex",Sex );
		map.put("Phone",Phone );
		map.put("Address",Address );
		map.put("Con",Con );
		map.put("certicode",certicode );
		return wfs.zxqzDownload(map,randomCode);
	}
	
	/**
	 * 网上咨询-我要留言
	 * @param request
	 */
	@RequestMapping(value = "gotoWyly" , method = RequestMethod.GET)
	public String gotoWyly(){
		return "wyly";
	}
	
	/**
	 * 进入我要留言页面
	 * @param request
	 */
	@RequestMapping(value = "wyly" , method = RequestMethod.POST)
	@ResponseBody
	public void wyly(HttpServletRequest request){
		String url = request.getParameter("url");
		wfs.wylyDownload(url);
	}
	
	/**
	 * 我要留言提交
	 * cc  20171024
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "wylySubmit" , method = RequestMethod.POST)
	@ResponseBody
	public String wylySubmit(HttpServletRequest request){
		Map<String, String> map = new HashMap<String, String>();
		String Name = request.getParameter("Name");
		String email = request.getParameter("email");
		String Phone = request.getParameter("Phone");
		String title = request.getParameter("title");
		String lybm = request.getParameter("lybm");
		String Con= request.getParameter("Con");
		map.put("Name",Name );
		map.put("email",email );
		map.put("Phone",Phone );
		map.put("title",title );
		map.put("lybm",lybm );
		map.put("Con",Con );
		return wfs.wylyDownload(map);
	}
	
	/**
	 * 我要留言状态页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "statusPage" , method = RequestMethod.GET)
	public ModelAndView statusPage(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
		String code = request.getParameter("code");
		map.put("code", code);
		return new ModelAndView("status",map);
	}
	
	/**
	 * 点击“我要举报单位”
	 * @return
	 */
	@RequestMapping(value = "jbdwPage", method = RequestMethod.GET)
	public String jbdwPage(){
		return "jbdw";
	}
	/**
	 * 点击“我要举报个人”
	 * @return
	 */
	@RequestMapping(value = "jbgrPage", method = RequestMethod.GET)
	public String jbgrPage(){
		return "jbgr";
	}
	/**
	 * 进入我要举报单位
	 * @param request
	 */
	@RequestMapping(value = "jbdw" , method = RequestMethod.POST)
	@ResponseBody
	public void jbdw(HttpServletRequest request){
		String url = request.getParameter("url");
		wfs.jbdwDownload(url);
	}
	
	/**
	 * 举报单位提交
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "jbdwSubmit" , method = RequestMethod.POST)
	@ResponseBody
	public int jbdwSubmit(HttpServletRequest request,HttpSession session){
		String xm = request.getParameter("xm");
		String szdw = request.getParameter("szdw");
		String zw = request.getParameter("zw");
		String lxzz = request.getParameter("lxzz");
		String yzbm = request.getParameter("yzbm");
		String lxdh= request.getParameter("lxdh");
		String sj= request.getParameter("sj");
		String email= request.getParameter("email");
		String dwmc_bjb= request.getParameter("dwmc_bjb");
		String zywt_bjb= request.getParameter("zywt_bjb");
		String certicode= request.getParameter("certicode");
		String randomCode = session.getAttribute("randomCode").toString();
		JuBao juBao = new JuBao();
		juBao.setXm(xm);
		juBao.setSzdw(szdw);
		juBao.setZw(zw);
		juBao.setLxzz(lxzz);
		juBao.setYzbm(yzbm);
		juBao.setLxdh(lxdh);
		juBao.setSj(sj);
		juBao.setEmail(email);
		juBao.setSzdw_bjb(dwmc_bjb);
		juBao.setZywt_bjb(zywt_bjb);
		juBao.setCerticode(certicode);
		return wfs.jbdwSubmitDownload(juBao,randomCode);
	}
	
	/**
	 * 进入我要举报个人
	 * @param request
	 */
	@RequestMapping(value = "jbgr" , method = RequestMethod.POST)
	@ResponseBody
	public void jbgr(HttpServletRequest request){
		String url = request.getParameter("url");
		wfs.jbgrDownload(url);
	}
	
	/**
	 * 举报个人提交
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "jbgrSubmit" , method = RequestMethod.POST)
	@ResponseBody
	public int jbgrSubmit(HttpServletRequest request,HttpSession session){
		String xm = request.getParameter("xm");
		String szdw = request.getParameter("szdw");
		String zw = request.getParameter("zw");
		String lxzz = request.getParameter("lxzz");
		String yzbm = request.getParameter("yzbm");
		String lxdh= request.getParameter("lxdh");
		String sj= request.getParameter("sj");
		String email= request.getParameter("email");
		String xm_bjb= request.getParameter("xm_bjb");
		String szdw_bjb= request.getParameter("szdw_bjb");
		String zw_bjb= request.getParameter("zw_bjb");
		String zywt_bjb= request.getParameter("zywt_bjb");
		String certicode= request.getParameter("certicode");
		String randomCode = session.getAttribute("randomCode").toString();
		JuBao juBao = new JuBao();
		juBao.setXm(xm);
		juBao.setSzdw(szdw);
		juBao.setZw(zw);
		juBao.setLxzz(lxzz);
		juBao.setYzbm(yzbm);
		juBao.setLxdh(lxdh);
		juBao.setSj(sj);
		juBao.setEmail(email);
		juBao.setXm_bjb(xm_bjb);;
		juBao.setSzdw_bjb(szdw_bjb);
		juBao.setZw_bjb(zw_bjb);;
		juBao.setZywt_bjb(zywt_bjb);
		juBao.setCerticode(certicode);
		return wfs.jbgrSubmitDownload(juBao,randomCode);
	}
	
	/**
	 * 提交成功页面
	 * @return
	 */
	@RequestMapping(value = "successPage" , method = RequestMethod.GET)
	public ModelAndView successPage(){
		return new ModelAndView("successPage");
	}
	
	/**
	 * 提交失败页面
	 * @return
	 */
	@RequestMapping(value = "failPage" , method = RequestMethod.GET)
	public ModelAndView failPage(){
		return new ModelAndView("failPage");
	}
	
	
	/**
	 * 铁岭公众互动页面获取验证码
	 * cc 20171127
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "tlgzhd" ,  method = RequestMethod.GET)
	@ResponseBody
	public void tlgzhd(HttpServletRequest request){
		String url = request.getParameter("url");
		System.out.println("url==>"+url);
		wfs.tlgzhddownload(url);
	}
	
	
	
}
