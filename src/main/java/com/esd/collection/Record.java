package com.esd.collection;

import java.util.Date;


public class Record {
	
	private String id;
	private String leve;//1:信息     2：警告   3：错误
	private String type;//1:静态  2：动态  3：交互  4：外链接
	private int status;
	private String statuStr;//1：成功，0：失败
	private String pgName;//规则文件名
	private String pgNode;//规则中错误节点
	private Date date;//存入日期时间到秒
	private String dateStr;//
	private String reason;//原因
	private String url;
	private String md5url;
	private String siteId;//站点id
	private int result;//处理结果 0. 不用处理  1. 已处理  2.待处理  3.处理中  4.搁置  5.现没有用
	private String note;//候选字段
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getLeve() {
		return leve;
	}
	public void setLeve(String leve) {
		this.leve = leve;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatuStr() {
		return statuStr;
	}
	public void setStatuStr(String statuStr) {
		this.statuStr = statuStr;
	}
	public String getPgName() {
		return pgName;
	}
	public void setPgName(String pgName) {
		this.pgName = pgName;
	}
	public String getPgNode() {
		return pgNode;
	}
	public void setPgNode(String pgNode) {
		this.pgNode = pgNode;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDateStr() {
		return dateStr;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getMd5url() {
		return md5url;
	}
	public void setMd5url(String md5url) {
		this.md5url = md5url;
	}
	
}
