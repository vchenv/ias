package com.esd.config;

import java.io.File;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.esd.collection.TaskTime;

public class BaseConfig extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static String[] str = {};

	public static String[] filterSuffix = { ".jpg", ".png", ".gif", ".exe",
			".jpeg", ".doc", ".docx", ".xlsx", ".xls", ".ppt", ".rar", ".zip",
			".pdf", "wbk", "xml" };
	
	
	public final static String SITENAME = "siteName";
	
	public final static String RECORD = "record";
	
	public final static String SITEID = "siteId";
	
	public final static String SITES = "sites";
	
	public final static String DOMAINNAME = "domainName";

	public static String ROOT;
	
	public static String ROOT1;

	public static String IMAGE_ROOT;

	public static String JS_ROOT;

	public static String CSS_ROOT;

	public static String PG_ROOT;

	public static String TEMPLATE_ROOT;

	public static String HTML_ROOT;

	public static String TEST_ROOT;
	
	public static String TEMP_ROOT;
	
//	public static final String[] INDEX_URL = { "http://www.caacca.org", "http://www.caacts.org.cn:8080" };
	public static final String[] INDEX_URL = { "http://www.szft.gov.cn/" };
	
	public static final String MESSAGE = "message";

	public static final String USER = "username";

	public static String time = "03:00:00";
	
	public static List<TaskTime> times = null;
	
	public static int webport = 8080;

	public void init() throws ServletException {
		ROOT = getServletContext().getRealPath("/") + "szft";
		//使用时需要        + siteName(站点名)
		ROOT1 = getServletContext().getRealPath("/");
		IMAGE_ROOT = ROOT + File.separator + "etc" + File.separator + "image";
		JS_ROOT = ROOT + File.separator + "etc" + File.separator + "js";
		CSS_ROOT = ROOT + File.separator + "etc" + File.separator + "styles";
		
		PG_ROOT = ROOT + File.separator + "db";
		TEMPLATE_ROOT = ROOT + File.separator + "template";
		HTML_ROOT = ROOT + File.separator + "html";
		TEST_ROOT = getServletContext().getRealPath("/");
		TEMP_ROOT = ROOT + File.separator + "temp";
	}

}
