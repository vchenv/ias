package com.esd.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.esd.collection.Record;
import com.esd.common.MongoDBUtil;
import com.esd.config.BaseConfig;
import com.esd.controller.site.SiteController;

@Controller
@RequestMapping("/record")
public class RecordController {
	@Resource
	private MongoDBUtil mongoDBUtil;
	
	@RequestMapping(value = "/record")
	public ModelAndView record() {
		
		return new ModelAndView("record");
		//return new ModelAndView("redirect:login");
	}
	
	@RequestMapping(value = "/getRecord", method = RequestMethod.POST)
	@ResponseBody
	public void getRecord(HttpServletRequest request,HttpServletResponse response) {
		System.out.println("进来了！record");
		Long total = mongoDBUtil.getRecordsCount(SiteController.siteId +"_"+BaseConfig.RECORD);
		//SiteController.siteId   +BaseConfig.RECORD
		List<Record> list = new ArrayList<Record>();
		list = mongoDBUtil.findAll(Record.class,"593685cb5ad87a74942c251c_record");
		JSONObject jobj = new JSONObject();
		jobj.accumulate("total",2968 );//total代表一共有多少数据  
        jobj.accumulate("rows", list);//row是代表显示的页的数据 

        response.setCharacterEncoding("utf-8");//指定为utf-8  
        try {
			response.getWriter().write(jobj.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}//转化为JSOn格式 
	}
	
	@RequestMapping(value = "/delRecord", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delRecord() {
		Map<String, Object> map = new HashMap<String, Object>();
		Long totle = mongoDBUtil.getRecordsCount(SiteController.siteId+"_"+BaseConfig.RECORD);
		List<Record> crs = mongoDBUtil.findAll(Record.class,SiteController.siteId+"_"+BaseConfig.RECORD);
		map.put("records", crs);
		map.put("totle", totle);
		return map;
	}
	@RequestMapping(value = "/updRecord", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updRecord() {
		Map<String, Object> map = new HashMap<String, Object>();
		Long totle = mongoDBUtil.getRecordsCount(SiteController.siteId+"_"+BaseConfig.RECORD);
		List<Record> crs = mongoDBUtil.findAll(Record.class,SiteController.siteId+"_"+BaseConfig.RECORD);
		map.put("records", crs);
		map.put("totle", totle);
		return map;
	}

}
