package com.esd.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.esd.collection.Site;
import com.esd.common.CatDao;
import com.esd.common.MongoDBUtil;
import com.esd.config.BaseConfig;
import com.esd.config.NodeConfig;
import com.esd.config.PageConfig;
import com.esd.download.EsdDownLoadHtml;
import com.esd.parser.Parser;
import com.esd.stuff.TemplateStuff;
import com.esd.util.Util;

@Controller
@RequestMapping("/admin/test")
public class TestConfigController {

	private static Logger log = Logger.getLogger(TestConfigController.class);
	@Resource
	private MongoDBUtil mongoDBUtil;
	@Resource
	private CatDao dao;
	/**
	 * 抓取预览方法
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value ="/view", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> view(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		long l = System.currentTimeMillis();
		String siteId = session.getAttribute("siteId").toString();
		//log.info("站点id：" + siteId);
		
		Criteria criatira = new Criteria();
		criatira.andOperator(Criteria.where("id").is(siteId));
		Site site = mongoDBUtil.findOneByCollectionName(BaseConfig.SITES, criatira, Site.class);	
		String[] domain = site.getDomainName().split(",");
		
		String url = request.getParameter("url");
		String templateName = request.getParameter("template");
		Document templateDoc = null;
		if (Util.isOutUrl(url,siteId,domain)) {// 网址为外部链接
			Util.doWithOutUrl(url,siteId,mongoDBUtil);
		} else {
			String javaScriptEnabled = request.getParameter("javaScriptEnabled");
			String sleep = request.getParameter("sleep");
			String[] rules = request.getParameterValues("rules[]");
			PageConfig pageConfig = new PageConfig();
			pageConfig.setJavaScriptEnabled(Boolean.valueOf(javaScriptEnabled));
			pageConfig.setSleep(Long.valueOf(sleep));
			pageConfig.setUrl(url);
			for (String s : rules) {
				String[] rule = s.split("&");
				NodeConfig nc = new NodeConfig();
				nc.setDes(rule[0]);
				nc.setParent(rule[1]);
				nc.setTag(rule[2]);
				nc.setType(rule[3]);
				nc.setName(rule[4]);
				nc.setIndex(rule[5]);
				nc.setAnchorId(rule[6]);
				pageConfig.getList().add(nc);
			}
			EsdDownLoadHtml down = new EsdDownLoadHtml();
			System.out.println(System.currentTimeMillis()-l);
			Document htmlSource = down.downloadHtml(pageConfig,dao,domain,mongoDBUtil);
			System.out.println("采集结束时间："+(System.currentTimeMillis()-l));
			//System.out.println("htmlSource:"+htmlSource.toString());
			Parser hp = new Parser();
			pageConfig = hp.ParserNode(htmlSource, pageConfig);
			TemplateStuff ts = new TemplateStuff();
			pageConfig.setTemplate(templateName);
			try {
				templateDoc = ts.templateStuff(pageConfig,siteId,mongoDBUtil);
//				log.debug("heml:"+templateDoc.html());
//				log.debug("url:"+BaseConfig.TEST_ROOT + File.separator + "view.html");
				//Util.createNewFile(templateDoc.html(), BaseConfig.TEST_ROOT + File.separator + "view.html");
			
			} catch (IOException e) {
				e.printStackTrace();
				
			}
			
		}
		log.debug("view finish");
		map.put("notice", true);
		map.put("html", templateDoc.html());
		return map;
	}
}
