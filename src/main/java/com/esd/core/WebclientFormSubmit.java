package com.esd.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;





















import com.esd.download.SimpleConectionListener;
import com.esd.verifyCode.demo1.AutoDiscern;
import com.esd.verifyCode.demo1.AutoDiscern2;
import com.esd.verifyCode.demo1.AutoDiscern3;
import com.gargoylesoftware.htmlunit.AlertHandler;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;

public class WebclientFormSubmit {
	private static Logger log = Logger.getLogger(WebclientFormSubmit.class);
	private static WebClient webClient = null;
	private String alertMessage = null;
	private BjmtgPage xxPage = new BjmtgPage();
	public static void main(String[] args) {
//		String url = "http://www.baidu.com";
//		Document document = Jsoup.parse("");
//		Elements elements = document.getElementsByTag("body");
//		String script = "<script type=\"text/javascript\">window.location.href='" + url + "'</script>";
//		elements.get(0).append(script);
//
//		System.out.println(document.html());
//		WebclientFormSubmit wb = new WebclientFormSubmit();
//		Map<String , String> map = new HashMap<String, String>();
//		map.put("name", "21212121");
//		map.put("email", "21212121@qq.com");
//		map.put("tel", "21212121");
//		map.put("title", "21212121");
//		map.put("content", "21212121");
//		wb.download(map,"http://www.bjmtg.gov.cn/hdpt/sjxx/");
	}

	public static void close() {
		if (webClient != null) {
			webClient.close();
		}
	}

	public WebclientFormSubmit() {
		if (webClient == null) {
			webClient = new WebClient(BrowserVersion.CHROME);
			new SimpleConectionListener(webClient);
		}
		
	}
	/**
	 * 加载信箱页面获取验证码
	 * @param url
	 */
	public void download(String url) {
		try {
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
		//	webClient.getOptions().setJavaScriptEnabled(false); // 启用JS解释器，默认为true
		//	webClient.getOptions().setCssEnabled(true); // 禁用css支持
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			// long l = System.currentTimeMillis();
			// HtmlPage htmlPage = webClient.getPage(pageConfig.getUrl());
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			//System.out.println(response.getContentType());// application/xhtml+xml
			log.debug(url+"===["+response.getContentType()+"]");
			// System.out.println(page.isHtmlPage());
			HtmlImage htmlImage = null;
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
				HtmlPage page1 = xxPage.getHtmlPage();
//				HtmlAnchor reShowVerifyCode = (HtmlAnchor) htmlPage.getElementById("reShowVerifyCode");
//				reShowVerifyCode.click();
				htmlImage = (HtmlImage) page1.getElementById("VerifyCodeImage");
				InputStream is = null;
				is = htmlImage.getWebResponse(true).getContentAsStream();
				byte[] s = AutoDiscern.image2byte(is);
				AutoDiscern autoDiscern = new AutoDiscern();
				String code = autoDiscern.discernPic(s);
				xxPage.setCode(code);
				is.close();
				log.info("进入页面方法code===>"+code);
			} 
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} 
	}
	
	/**
	 * 书记、区长邮箱提交
	 * @param map
	 * @return
	 */
	public int download(Map<String,String> map,String randomCode) {
		HtmlPage htmlPage = xxPage.getHtmlPage();
		try {
				
				if (!randomCode.equals(map.get("certicode"))) {
					return 0;
				}
				HtmlInput nameInput = (HtmlInput) htmlPage.getElementById("name");
				nameInput.setValueAttribute(map.get("name"));
				HtmlInput emailInput = (HtmlInput) htmlPage.getElementById("email");
				emailInput.setValueAttribute(map.get("email"));
				HtmlInput telInput = (HtmlInput) htmlPage.getElementById("tel");
				telInput.setValueAttribute(map.get("tel"));
				HtmlInput titleInput = (HtmlInput) htmlPage.getElementById("title");
				titleInput.setValueAttribute(map.get("title"));
				HtmlTextArea contentTextArea = (HtmlTextArea) htmlPage.getElementById("content");
				contentTextArea.setTextContent(map.get("content"));
				
				long t1 = System.currentTimeMillis();
				long t2 = System.currentTimeMillis();
				String code = null;
				while ((t2 - t1) < 10000) {
					t2 = System.currentTimeMillis();
					code = xxPage.getCode();
					if(code != null && code !=""){
						break;
					}
				}
				log.info("点击提交方法code===>"+code);
				HtmlInput yanzhengmaInput = (HtmlInput) htmlPage.getElementById("yanzhengma");
				yanzhengmaInput.setValueAttribute(code);
				HtmlInput SubmitButtonInput = (HtmlInput) htmlPage.getElementById("SubmitButton");
				HtmlPage endPage = null;
				endPage = SubmitButtonInput.click();
				System.out.println("endPage==>"+endPage);
				List<HtmlElement> successDivs = endPage.getBody().getElementsByAttribute("div", "style", "font-size: 12pt; line-heigth: 35px;");
				List<HtmlElement> faileSpans = endPage.getBody().getElementsByAttribute("span", "style", "color: red");
				if(successDivs.size() != 0){
					return 1;
				}
				if(faileSpans.size() != 0){
					return 0;
				}
				xxPage = new BjmtgPage();
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}finally{
			if (webClient != null) {
				webClient.close();
			}
		}
		return 2;
	}

	
	/**
	 * 在线求助
	 * @param map
	 * @return
	 */
	public void zxqzDownload(String url) {
		try {
		
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			// long l = System.currentTimeMillis();
			// HtmlPage htmlPage = webClient.getPage(pageConfig.getUrl());
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			//System.out.println(response.getContentType());// application/xhtml+xml
			log.debug(url+"===["+response.getContentType()+"]");
			// System.out.println(page.isHtmlPage());
			HtmlImage htmlImage = null;
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
				HtmlPage htmlPage = xxPage.getHtmlPage();
				htmlImage = (HtmlImage)htmlPage.getElementById("imgyz");
//				System.out.println(htmlImage.asXml());
				InputStream is = null;
				is = htmlImage.getWebResponse(true).getContentAsStream();
				AutoDiscern2 discern = new AutoDiscern2();
				BufferedImage bi = ImageIO.read(is);
				String code = discern.discernPic(bi, 0);
				xxPage.setCode(code);
//				byte[] s = AutoDiscern.image2byte(is);
//				AutoDiscern autoDiscern = new AutoDiscern();
//				String code = autoDiscern.discernPic(s);
				is.close();
				log.info("进入页面方法code===>"+code);
			}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} 
	}
	/**
	 * 在线求助
	 * @param map
	 * @return
	 */
	public int zxqzDownload(Map<String, String> map,String randomCode) {
		log.info("点击提交！");
		int i = 0;
		HtmlPage htmlPage = xxPage.getHtmlPage();
		try {
			webClient.setAlertHandler(new AlertHandler() {
				public void handleAlert(Page htmlPage, String message) {
					alertMessage = message;
					log.error("***************************************");
					log.error("handleAlert:" + alertMessage);
					log.error("***************************************");
				}
			});
			
				if (!randomCode.equals(map.get("certicode"))) {
					return 0;
				}
				HtmlSelect nimingSelect = (HtmlSelect) htmlPage.getElementById("isNiMing");
				HtmlOption nimingOption = nimingSelect.getOptionByValue(map.get("isNiMing"));
				nimingOption.setSelected(true);
				HtmlInput nameInput = (HtmlInput) htmlPage.getElementById("Name");
				nameInput.setValueAttribute(map.get("Name"));
				HtmlSelect sexSelect = (HtmlSelect) htmlPage.getElementById("Sex");
				HtmlOption sexOption = sexSelect.getOptionByValue(map.get("Sex"));
				sexOption.setSelected(true);
				HtmlInput phoneInput = (HtmlInput) htmlPage.getElementById("Phone");
				phoneInput.setValueAttribute(map.get("Phone"));
				HtmlInput addressInput = (HtmlInput) htmlPage.getElementById("Address");
				addressInput.setValueAttribute(map.get("Address"));
				HtmlTextArea contentTextArea = (HtmlTextArea) htmlPage.getElementById("Content");
				contentTextArea.setTextContent(map.get("Con"));
				long t1 = System.currentTimeMillis();
				long t2 = System.currentTimeMillis();
				String code = null;
				while ((t2 - t1) < 60000) {
					t2 = System.currentTimeMillis();
					code = xxPage.getCode();
					if(code != null && code !=""){
						break;
					}
				}
				log.info("点击提交方法code===>"+code);
				HtmlInput yzmInput = (HtmlInput) htmlPage.getElementById("yzm");
				yzmInput.setValueAttribute(code);
				HtmlInput SubmitButtonInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "name", "Button1").get(0);
				SubmitButtonInput.click();
//				System.out.println("endPage==>"+endPage);
				
				
				if(alertMessage.contains("错误")){
					i=0;
				}
				if(alertMessage.contains("成功")){
					i=1;
				}
//				System.out.println("alertMessage==>"+alertMessage);
				alertMessage = null;
				xxPage = new BjmtgPage();
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} finally {
			if (webClient != null) {
				webClient.close();
			}
		}
		return i;
	}

	/**
	 * 南昌查询
	 * @param keyWord
	 * @return
	 */
	public Map<String, Object> nanchangSearch(String keyWord) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> contentList = new ArrayList<Map<String,Object>>();
		System.out.println("南昌搜索方法");
		for (int i = 1; i < 4; i++) {
			Map<String, Object> map1 = new HashMap<String, Object>();
			map1.put("title", "标题"+i);
			map1.put("href", "链接"+i);
			map1.put("date", "日期"+i);
			contentList.add(map1);
		}
		map.put("keyWord", keyWord);
		map.put("list", contentList);
		return map;
	}

	/**
	 * 加载我要留言页面
	 * @param url
	 * @return
	 */
	public void wylyDownload(String url) {
		try {
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			log.debug(url+"===["+response.getContentType()+"]");
			// System.out.println(page.isHtmlPage());
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
			}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		}
	}

	/**
	 * 我要留言提交
	 * @param map
	 * @return
	 */
	public String wylyDownload(Map<String, String> map) {
		HtmlPage htmlPage = xxPage.getHtmlPage();
		String code = null;
		try {
				HtmlInput nameInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "name", "lyname").get(0);
				nameInput.setValueAttribute(map.get("Name"));
				HtmlInput emailInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "name", "lymail").get(0);
				emailInput.setValueAttribute(map.get("email"));
				HtmlInput telInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "name", "lytel").get(0);
				telInput.setValueAttribute(map.get("Phone"));
				HtmlInput titleInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "name", "lytitle").get(0);
				titleInput.setValueAttribute(map.get("title"));
				HtmlSelect sexSelect = (HtmlSelect) htmlPage.getBody().getElementsByAttribute("select", "name", "lybm").get(0);
				HtmlOption sexOption = sexSelect.getOptionByValue(map.get("lybm"));
				sexOption.setSelected(true);
				HtmlTextArea contentTextArea = (HtmlTextArea) htmlPage.getBody().getElementsByAttribute("textarea", "name", "lymain").get(0);
				contentTextArea.setTextContent(map.get("Con"));
				
				HtmlInput SubmitButtonInput = (HtmlInput) htmlPage.getBody().getElementsByAttribute("input", "value", "留 言").get(0);
				HtmlPage endPage = null;
				endPage = SubmitButtonInput.click();
				code = endPage.getElementById("Label3").asText();
				System.out.println("endPage==>"+endPage);
				System.out.println("-----------------------------------------------------------");
				System.out.println("code==>"+code);
				
				
				xxPage = new BjmtgPage();
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}finally{
			if (webClient != null) {
				webClient.close();
			}
		}
		return code;
	}

	/**
	 * 进入我要举报单位
	 * @param url
	 */
	public void jbdwDownload(String url) {
		try {
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			// long l = System.currentTimeMillis();
			// HtmlPage htmlPage = webClient.getPage(pageConfig.getUrl());
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			//System.out.println(response.getContentType());// application/xhtml+xml
			log.debug(url+"===["+response.getContentType()+"]");
			// System.out.println(page.isHtmlPage());
			HtmlImage htmlImage = null;
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
				HtmlPage htmlPage = xxPage.getHtmlPage();
				htmlImage = (HtmlImage)htmlPage.getElementById("VerifyCodeImage");
				InputStream is = null;
				is = htmlImage.getWebResponse(true).getContentAsStream();
				byte[] s = AutoDiscern.image2byte(is);
				AutoDiscern3 discern = new AutoDiscern3();
				String code = discern.discernPic(s);
				xxPage.setCode(code);
				is.close();
				log.info("进入页面方法code===>"+code);
			}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} 
		
	}

	/**
	 * 举报单位提交
	 * @param juBao
	 * @return
	 */
	public int jbdwSubmitDownload(JuBao juBao,String randomCode) {
		log.info("点击提交！");
		HtmlPage htmlPage = xxPage.getHtmlPage();
		try {
			if (!randomCode.equals(juBao.getCerticode())) {
				return 0;
			}
				HtmlInput xmInput = (HtmlInput) htmlPage.getElementById("xm");
				xmInput.setValueAttribute(juBao.getXm());
				HtmlInput szdwInput = (HtmlInput) htmlPage.getElementById("szdw");
				szdwInput.setValueAttribute(juBao.getSzdw());
				HtmlInput zwInput = (HtmlInput) htmlPage.getElementById("zw");
				zwInput.setValueAttribute(juBao.getZw());
				HtmlInput lxzzInput = (HtmlInput) htmlPage.getElementById("lxzz");
				lxzzInput.setValueAttribute(juBao.getLxzz());
				HtmlInput yzbmInput = (HtmlInput) htmlPage.getElementById("yzbm");
				yzbmInput.setValueAttribute(juBao.getYzbm());
				HtmlInput lxdhInput = (HtmlInput) htmlPage.getElementById("lxdh");
				lxdhInput.setValueAttribute(juBao.getLxdh());
				HtmlInput sjInput = (HtmlInput) htmlPage.getElementById("sj");
				sjInput.setValueAttribute(juBao.getSj());
				HtmlInput emailInput = (HtmlInput) htmlPage.getElementById("email");
				emailInput.setValueAttribute(juBao.getEmail());
				HtmlInput dwmc_bjbInput = (HtmlInput) htmlPage.getElementById("dwmc_bjb");
				dwmc_bjbInput.setValueAttribute(juBao.getSzdw_bjb());
				HtmlTextArea zywt_bjbTextArea = (HtmlTextArea) htmlPage.getElementById("zywt_bjb");
				zywt_bjbTextArea.setTextContent(juBao.getZywt_bjb());
				
				long t1 = System.currentTimeMillis();
				long t2 = System.currentTimeMillis();
				String code = null;
				while ((t2 - t1) < 60000) {
					t2 = System.currentTimeMillis();
					code = xxPage.getCode();
					if(code != null && code !=""){
						break;
					}
				}
				log.info("点击提交方法code===>"+code);
				HtmlInput yanzhengmaInput = (HtmlInput) htmlPage.getElementById("yanzhengma");
				yanzhengmaInput.setValueAttribute(xxPage.getCode());
				HtmlInput SubmitButtonInput = (HtmlInput) htmlPage.getElementById("SubmitButton");
				HtmlPage endPage = null;
				endPage = SubmitButtonInput.click();
				System.out.println("endPage==>"+endPage);
				List<HtmlElement> successDivs = endPage.getBody().getElementsByAttribute("div", "style", "font-size: 12pt; line-heigth: 35px;");
				List<HtmlElement> faileSpans = endPage.getBody().getElementsByAttribute("span", "style", "color: red");
				if(successDivs.size() != 0){
					return 1;
				}
				if(faileSpans.size() != 0){
					return 0;
				}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} finally {
			if (webClient != null) {
				webClient.close();
			}
		}
		return 1;
	}

	/**
	 * 进入我要举报个人
	 * @param url
	 */
	public void jbgrDownload(String url) {
		try {
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			// long l = System.currentTimeMillis();
			// HtmlPage htmlPage = webClient.getPage(pageConfig.getUrl());
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			//System.out.println(response.getContentType());// application/xhtml+xml
			log.debug(url+"===["+response.getContentType()+"]");
			// System.out.println(page.isHtmlPage());
			HtmlImage htmlImage = null;
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
				HtmlPage htmlPage = xxPage.getHtmlPage();
				htmlImage = (HtmlImage)htmlPage.getElementById("VerifyCodeImage");
				InputStream is = null;
				is = htmlImage.getWebResponse(true).getContentAsStream();
				byte[] s = AutoDiscern.image2byte(is);
				AutoDiscern3 discern = new AutoDiscern3();
				String code = discern.discernPic(s);
				xxPage.setCode(code);
				is.close();
				log.info("进入页面方法code===>"+code);
			}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} 
	}

	/**
	 * 举报个人提交
	 * @param juBao
	 * @return
	 */
	public int jbgrSubmitDownload(JuBao juBao,String randomCode) {
		log.info("点击提交！");
		HtmlPage htmlPage = xxPage.getHtmlPage();
		try {
				if (!randomCode.equals(juBao.getCerticode())) {
					return 0;
				}
				HtmlInput xmInput = (HtmlInput) htmlPage.getElementById("xm");
				xmInput.setValueAttribute(juBao.getXm());
				HtmlInput szdwInput = (HtmlInput) htmlPage.getElementById("szdw");
				szdwInput.setValueAttribute(juBao.getSzdw());
				HtmlInput zwInput = (HtmlInput) htmlPage.getElementById("zw");
				zwInput.setValueAttribute(juBao.getZw());
				HtmlInput lxzzInput = (HtmlInput) htmlPage.getElementById("lxzz");
				lxzzInput.setValueAttribute(juBao.getLxzz());
				HtmlInput yzbmInput = (HtmlInput) htmlPage.getElementById("yzbm");
				yzbmInput.setValueAttribute(juBao.getYzbm());
				HtmlInput lxdhInput = (HtmlInput) htmlPage.getElementById("lxdh");
				lxdhInput.setValueAttribute(juBao.getLxdh());
				HtmlInput sjInput = (HtmlInput) htmlPage.getElementById("sj");
				sjInput.setValueAttribute(juBao.getSj());
				HtmlInput emailInput = (HtmlInput) htmlPage.getElementById("email");
				emailInput.setValueAttribute(juBao.getEmail());
				HtmlInput xm_bjbInput = (HtmlInput) htmlPage.getElementById("xm_bjb");
				xm_bjbInput.setValueAttribute(juBao.getXm_bjb());
				HtmlInput szdw_bjbInput = (HtmlInput) htmlPage.getElementById("szdw_bjb");
				szdw_bjbInput.setValueAttribute(juBao.getSzdw_bjb());
				HtmlTextArea zywt_bjbTextArea = (HtmlTextArea) htmlPage.getElementById("zywt_bjb");
				zywt_bjbTextArea.setTextContent(juBao.getZywt_bjb());
				
				long t1 = System.currentTimeMillis();
				long t2 = System.currentTimeMillis();
				String code = null;
				while ((t2 - t1) < 60000) {
					t2 = System.currentTimeMillis();
					code = xxPage.getCode();
					if(code != null && code !=""){
						break;
					}
				}
				log.info("点击提交方法code===>"+code);
				HtmlInput yanzhengmaInput = (HtmlInput) htmlPage.getElementById("yanzhengma");
				yanzhengmaInput.setValueAttribute(xxPage.getCode());
				HtmlInput SubmitButtonInput = (HtmlInput) htmlPage.getElementById("SubmitButton");
				HtmlPage endPage = null;
				endPage = SubmitButtonInput.click();
				System.out.println("endPage==>"+endPage);
				List<HtmlElement> successDivs = endPage.getBody().getElementsByAttribute("div", "style", "font-size: 12pt; line-heigth: 35px;");
				List<HtmlElement> faileSpans = endPage.getBody().getElementsByAttribute("span", "style", "color: red");
				if(successDivs.size() != 0){
					return 1;
				}
				if(faileSpans.size() != 0){
					return 0;
				}
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} finally {
			if (webClient != null) {
				webClient.close();
			}
		}
		return 1;
	}

	
	/**
	 * 铁岭公众互动页面验证码
	 * @param url
	 */
	public void tlgzhddownload(String url) {
		try {
			webClient.getOptions().setJavaScriptEnabled(true); // 启用JS解释器，默认为true
			webClient.getOptions().setCssEnabled(false); // 禁用css支持
			webClient.getOptions().setThrowExceptionOnScriptError(false); // js运行错误时，是否抛出异常
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.getOptions().setTimeout(10000); // 设置连接超时时间
			WebWindow webWindos = null;
			WebWindow w0 = webClient.getWebWindows().get(0);
			webWindos = webClient.openWindow(new URL(url), "page", w0);
			Page page = webWindos.getEnclosedPage();
			WebResponse response = page.getWebResponse();
			log.debug(url+"===["+response.getContentType()+"]");
			HtmlImage htmlImage = null;
			if (page.isHtmlPage()) {
				xxPage.setHtmlPage((HtmlPage) page);
				HtmlPage page1 = xxPage.getHtmlPage();
				htmlImage = (HtmlImage) page1.getElementById("code");
				InputStream is = null;
				is = htmlImage.getWebResponse(true).getContentAsStream();
				byte[] s = AutoDiscern.image2byte(is);
				AutoDiscern autoDiscern = new AutoDiscern();
				String code = autoDiscern.discernPic(s);
				xxPage.setCode(code);
				is.close();
				log.info("进入页面方法code===>"+code);
			} 
		} catch (FailingHttpStatusCodeException e) {
			log.error(e);
		} catch (MalformedURLException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} 
		
	}

	

}
