package com.esd.download;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.FalsifyingWebConnection;

public class SimpleConectionListener extends FalsifyingWebConnection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8168493020295282906L;

	private static Logger log = Logger.getLogger(SimpleConectionListener.class);


	private List<String> filterRes = null;
	
	@Value("${filterStr}")
	private String filterStr;
	
	public SimpleConectionListener(WebClient webClient) {
		super(webClient);
	}

	@Override
	public synchronized WebResponse getResponse(WebRequest request) throws IOException {
		WebResponse response = super.getResponse(request);
		String url = response.getWebRequest().getUrl().toString();
		long time = response.getLoadTime();
		String type = response.getContentType();
		int statusCode = response.getStatusCode();
		String msg = response.getStatusMessage();
		response.getWebRequest().getEncodingType();
		log.info("[url:" + url + "][time:" + time + "][type:" + type + "][statusCode:" + statusCode + "][msg:" + msg
				+ "]");

		if (url.indexOf("www.google-analytics.com")!=-1) {
			//return createWebResponse(response.getWebRequest(), "", "application/javascript", 200, "Ok");
			//return createWebResponse(response.getWebRequest(), "", "image/gif", 200, "Ok");
		}
		
		if (type.equals("text/css")) {
	

		}

		handle(url);
		return response;
	}

	public void handle(String url) {

	}

	@Override
	protected WebResponse createWebResponse(WebRequest wr, String content, String contentType) throws IOException {

		return super.createWebResponse(wr, content, contentType);
	}

	@Override
	protected WebResponse createWebResponse(WebRequest wr, String content, String contentType, int responseCode,
			String responseMessage) throws IOException {
		log.info("=======================createWebResponse=================================================");
		log.info("[content:" + content + "][contentType:" + contentType + "]" + "[responseCode:" + responseCode + "]"
				+ "[responseMessage:" + responseMessage + "]");
		return super.createWebResponse(wr, content, contentType, responseCode, responseMessage);
	}

	@Override
	protected WebResponse replaceContent(WebResponse wr, String newContent) throws IOException {
		log.info("=======================replaceContent=================================================");
		log.info("[wr:" + wr + "][newContent" + newContent + "]");
		return super.replaceContent(wr, newContent);
	}

	@Override
	protected WebResponse deliverFromAlternateUrl(WebRequest webRequest, URL url) throws IOException {
		log.info("=======================deliverFromAlternateUrl=================================================");
		log.info("[webRequest:" + webRequest + "][url" + url + "]");
		return super.deliverFromAlternateUrl(webRequest, url);
	}

}
