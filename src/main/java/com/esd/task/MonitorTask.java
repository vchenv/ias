package com.esd.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.esd.collection.TaskTime;
import com.esd.config.BaseConfig;
import com.esd.controller.site.SiteController;
import com.esd.core.CollectionPage;

/**
 * 定时任务
 * 
 * @author cx 20170626
 * 
 */
@Component
public class MonitorTask {

	@Resource
	private CollectionPage collectionPage;
	/**
	 * 20170626 多時間定時
	 */
	@Scheduled(cron = "* * * * * ? ")
	public void timerTask() {
		String time = BaseConfig.time;
		if(time != null && time.trim().length() > 0){
			List<TaskTime> times = BaseConfig.times;
			if(times != null){
				for (Iterator<TaskTime> iterator = times.iterator(); iterator.hasNext();) {
					TaskTime taskTime = (TaskTime) iterator.next();
					if (new SimpleDateFormat("HH:mm:ss").format(new Date()).equals(taskTime.getTime())) {
						collectionPage.init(BaseConfig.INDEX_URL[0],SiteController.siteId);
						collectionPage.start();
					}
				}
			}
		}
	}
}