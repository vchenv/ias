package com.esd.verifyCode.demo1;

import java.net.URL;

public class Demo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		URL path = Demo.class.getResource("");
		byte[] s = AutoDiscern.image2byte(path.getPath()+"tmp.bmp");
		AutoDiscern autoDiscern = new AutoDiscern();
		String code = autoDiscern.discernPic(s);
		System.out.println(code);

	}

}
