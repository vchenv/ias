<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${contextPath}/css/zxqz.css" />
<link rel="stylesheet" type="text/css"
	href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css"
	href="/iac/iac/css?name=index.css" />
<title>换届举报平台</title>
<script type="text/javascript">
	function jbdw() {
		window.location.href = "jbdwPage";
	}

	function jbgr() {
		window.location.href = "jbgrPage";
	}

	$(function() {
		$.ajax({
			url : 'showTemplate?fileName=header.html',
			type : 'GET',
			sync : false,
			success : function(data) {
				$("#header").append(data);
			}
		});

		$.ajax({
			url : 'showTemplate?fileName=footer.html',
			type : 'GET',
			sync : false,
			success : function(data) {
				$("#footer").append(data);
			}
		});
	})
</script>
<style type="text/css">
.jb_btn {
	
	text-align: center;
	height: 40px;
	width: 206px;
	display: inline;
	margin: 0px 50px 0px 50px;
}
.jb_btn a{
	 height: 40px;
     line-height: 40px;
     padding: 0 20px;
     background: #2298ef;
     border: 1px #2298ef solid;
     border-radius: 3px;
     color: #fff;
     display: inline-block;
     text-decoration: none;
     font-size: 16px;
     outline: none;
}

</style>

</head>
<body>
	<div id="header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>举报须知</h2>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="wxts">
					</br> </br> &nbsp;&nbsp;&nbsp;&nbsp;您已选择向中共门头沟区委组织部提交举报，请阅读以下举报须知。 </br> </br>
					&nbsp;&nbsp;&nbsp;&nbsp;1.本举报网站主要受理违反换届纪律问题或违规选人用人问题的举报。 </br> </br>
					&nbsp;&nbsp;&nbsp;&nbsp;2.请认真填写举报表单中的各项目。填写“主要问题”一项时，要写清楚问题发生的时间、地点、情节、后果等，字数限定在800字以内。
					</br> </br>
					&nbsp;&nbsp;&nbsp;&nbsp;3.举报人应实事求是地反映问题。对利用举报捏造事实，进行诬告陷害的，依照有关规定严肃处理；构成犯罪的，移交司法机关处理。
					</br> </br> &nbsp;&nbsp;&nbsp;&nbsp;4.提倡实名举报。进行实名举报的，请详细填写联系电话等项目。 </br> </br>
				</div>

				<div class="xx_btns">
					<!--	<div class="jb_btn">
						<a  href="http://www.bjmtg.gov.cn/hjjb/jbdw/" id="jbdw">我要举报单位</a>
					</div>
					<div class="jb_btn">
						<a  href="http://www.bjmtg.gov.cn/hjjb/jbgr/" id="jbgr">我要举报个人</a>
					</div> -->
				 <button type="button" class="upd_pwd_btn" onclick="jbdw()"
						title="我要举报单位">我要举报单位</button>
					<button type="button" class="upd_pwd_btn" onclick="jbgr()"
						title="我要举报个人">我要举报个人</button>
				</div>
			</div>
		</div>
	</div>
	<div id="footer"></div>
</body>
</html>
