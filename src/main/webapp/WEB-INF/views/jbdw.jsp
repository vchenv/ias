<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/public.js"></script>
<script type="text/javascript" src="${contextPath}/etc/tool/Voice.js"></script>
<link rel="stylesheet" type="text/css"
	href="${contextPath}/css/zxqz.css" />
<link rel="stylesheet" type="text/css"
	href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css"
	href="/iac/iac/css?name=index.css" />
<title>举报单位</title>
<script type="text/javascript">
$.ajaxSetup({
	cache : false
});
$(document).ready(function() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
	$("#verifyImage").load(function() {
		var timestamp = new Date().getTime();
		$.ajax({
			type : 'get',
			url : '../iac/random/getCode?' + timestamp,
			dataType : 'json',
			success : function(data) {
				$("#voicepath").val(data.path);
				// alert(data.path);
				// return data;
			}
		});
	});
	$("#verifyImage").click(function() {
		var timestamp = new Date().getTime();
		$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);

	});
});
function changeImage() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
}




	$(function(){
		var url = "http://www.bjmtg.gov.cn/hjjb/jbdw/";
		$.ajax({
			url:'jbdw',
			type:'POST',
			sync: true,
			data:{
				"url":url
			},
			success:function(){
				
			}
		});
	})

	function jbdwSubmit(){
		var pattern = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]");
		var xm = $("#xm").val();
		for (var i = 0; i < xm.length; i++) {
			if(pattern.test(xm.substr(i,1))){
				alert("姓名中请不要输入特殊符号");
			    return false;
			}
		}
		var szdw = $("#szdw").val();
		for (var i = 0; i < szdw.length; i++) {
			if(pattern.test(szdw.substr(i,1))){
				alert("所在单位中请不要输入特殊符号");
			    return false;
			}
		}
		var zw = $("#zw").val();
		for (var i = 0; i < zw.length; i++) {
			if(pattern.test(zw.substr(i,1))){
				alert("职务中请不要输入特殊符号");
			    return false;
			}
		}
		var lxzz = $("#lxzz").val();
		for (var i = 0; i < lxzz.length; i++) {
			if(pattern.test(lxzz.substr(i,1))){
				alert("联系地址中请不要输入特殊符号");
			    return false;
			}
		}
		var yzbm = $("#yzbm").val();
		for (var i = 0; i < yzbm.length; i++) {
			if(pattern.test(yzbm.substr(i,1))){
				alert("邮政编码中请不要输入特殊符号");
			    return false;
			}
		}
		var lxdh = $("#lxdh").val();
		for (var i = 0; i < lxdh.length; i++) {
			if(pattern.test(lxdh.substr(i,1))){
				alert("联系电话中请不要输入特殊符号");
			    return false;
			}
		}
		var sj = $("#sj").val();
		for (var i = 0; i < sj.length; i++) {
			if(pattern.test(sj.substr(i,1))){
				alert("手机中请不要输入特殊符号");
			    return false;
			}
		}
		var email = $("#email").val();
		for (var i = 0; i < email.length; i++) {
			if(pattern.test(email.substr(i,1))){
				alert("邮箱中请不要输入特殊符号");
			    return false;
			}
		}
		
		var dwmc_bjb = $("#dwmc_bjb").val();
		if(dwmc_bjb == ""){
			alert('被举报单位名称不能为空');
			return false;
			}
		for (var i = 0; i < dwmc_bjb.length; i++) {
			if(pattern.test(dwmc_bjb.substr(i,1))){
				alert("被举报单位名称中请不要输入特殊符号");
			    return false;
			}
		}
		
		var zywt_bjb = $("#zywt_bjb").val();
		if(zywt_bjb == ""){
			alert('主要问题不能为空');
			return false;
			}
		for (var i = 0; i < zywt_bjb.length; i++) {
			if(pattern.test(zywt_bjb.substr(i,1))){
				alert("主要问题中请不要输入特殊符号");
			    return false;
			}
		}
		
		var certicode = $("#certicode").val();
		if(certicode == ""){
			alert('验证码不能为空');
			return false;
			}
		for (var i = 0; i < certicode.length; i++) {
			if(pattern.test(certicode.substr(i,1))){
				alert("验证码中请不要输入特殊符号");
			    return false;
			}
		}
		$.ajax({
			url:'jbdwSubmit',
			type:'POST',
			sync: true,
			data:{
				"xm":xm,
				"szdw":szdw,
				"zw":zw,
				"lxzz":lxzz,
				"yzbm":yzbm,
				"lxdh":lxdh,
				"sj":sj,
				"email":email,
				"dwmc_bjb":dwmc_bjb,
				"zywt_bjb":zywt_bjb,
				"certicode":certicode
			},
			success:function(data){
				if(data == 0){
					$(".jb_main").attr("style","display:none;")
					$(".jubao").append("</br></br></br>"+
							"<img src='http://www.bjmtg.gov.cn/infogate/customer/images/icon_alert_error.gif' border='0' alt='表单提交失败'></br></br>"+
							"<span style='font-size: 20px; color: red;'><b>表单提交失败</b></span></br></br></br>"+
							"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
				}
				if(data == 1){
					$(".jb_main").attr("style","display:none;")
					$(".jubao").append("</br></br></br>"+
							"<img src='http://www.bjmtg.gov.cn/infogate/customer/images/icon_alert_success.gif' border='0' alt='表单提交成功'></br></br>"+
							"<span style='font-size: 20px; color: red;'><b>表单提交成功</b></span></br></br></br>"+
							"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
				}
				
			}
		});
	}
	
	function sx(){
		location.reload();
	}
	
	function cx(){
		document.getElementById("xm").value="";
		document.getElementById("szdw").value="";
		document.getElementById("zw").value="";
		document.getElementById("lxzz").value="";
		document.getElementById("yzbm").value="";
		document.getElementById("lxdh").value="";
		document.getElementById("sj").value="";
		document.getElementById("email").value="";
		document.getElementById("dwmc_bjb").value="";
		document.getElementById("zywt_bjb").value="";
		document.getElementById("certicode").value="";
		 
	}

	$(function() {
		$.ajax({
			url : 'showTemplate?fileName=header.html',
			type : 'GET',
			sync : false,
			success : function(data) {
				$("#header").append(data);
			}
		});

		$.ajax({
			url : 'showTemplate?fileName=footer.html',
			type : 'GET',
			sync : false,
			success : function(data) {
				$("#footer").append(data);
			}
		});
	})
</script>
<style type="text/css">
.jubao { overflow:hidden; margin-top: 70px ;clear: both;}
.jubao .title { background:url(jb_pic20160421.gif) no-repeat bottom center; width:272px; text-align:center; height:40px; line-height:40px; font-size:30px; font-family:"微软雅黑"; margin:0 auto; height:60px; font-weight:bold; color:#1171D8; margin-top:30px}
.jubao .jb_main { width:924px; overflow:hidden; padding-top:0px; margin:0px auto 40px }
.jubao .jb_main .jb_left { width:380px; border:1px #D0D0D0 solid; float:left; padding:20px 25px; height:382px;}
.jubao .jb_main .jb_middle { width:59px; text-align:center; float:left; padding-top:204px;}
.jubao .jb_main .jb_right { width:380px; border:1px #D0D0D0 solid; float:right; padding:20px 25px; height:382px;}
.jubao .jb_main .jb_title_l { border-bottom:2px #0D6FD4 solid; color:#0D6FD4; font-size:18px; font-weight:bold; line-height:40px; font-family:"微软雅黑"; margin-bottom:10px}
.jubao .jb_main .jb_title_r { border-bottom:2px #C50303 solid; color:#C50303; font-size:18px; font-weight:bold; line-height:40px; font-family:"微软雅黑"; margin-bottom:10px}
.jubao .jb_main .jb_list { overflow:hidden}
.jubao .jb_main .jb_list dl { clear:both; margin-top:10px; line-height:30px; overflow:hidden}
.jubao .jb_main .jb_list dt { width:105px; float:left; background-color:#EEEEEE; text-align:right; font-size:12px; font-family:"微软雅黑";}
.jubao .jb_main .jb_list dd { width:250px; float:left; padding-left:15px}
.jubao .jb_main .jb_list dd input { border:1px #D0D0D0 solid; line-height:26px; height:26px; width:246px; padding-left:4px;}
.jubao .jb_main .txt1 {border:1px #D0D0D0 solid; line-height:22px; width:246px; height:143px;}
.jubao .jb_main .txt2 {border:1px #D0D0D0 solid; line-height:22px; width:246px; height:224px;}
.jubao .jb_main .sub { text-align:center; margin-top:50px;}
.jubao .jb_main .submit { background:url(jb_sub20160421.gif) no-repeat; width:100px; height:34px; line-height:34px; color:#FFFFFF; border:none; cursor:pointer}
.clear { clear:both}
</style>
</head>
<body>
	<div id="header"></div>
	<div class="jubao">
		<div class="jb_main">
			<div class="jb_title" style="font-size: 28px;font-family:'微软雅黑'; font-weight:bold; color: #1171D8;text-align: center; margin-bottom: 40px;" >
				举报单位
			</div>

			<div class="jb_left">
				<div class="jb_title_l">举报人信息</div>
				<div class="jb_list">
					<dl>
						<dt>姓名：</dt>
						<dd>
							<input type="text" id="xm" name="xm" placeholder="姓名" title="姓名">
						</dd>
					</dl>
					<dl>
						<dt>所在单位：</dt>
						<dd>
							<input type="text" id="szdw" name="szdw" placeholder="所在单位" title="所在单位">
						</dd>
					</dl>
					<dl>
						<dt>职务：</dt>
						<dd>
							<input type="text" id="zw" name="zw" placeholder="职务" title="职务">
						</dd>
					</dl>
					<dl>
						<dt>联系地址：</dt>
						<dd>
							<input type="text" id="lxzz" name="lxzz" placeholder="联系地址" title="联系地址">
						</dd>
					</dl>
					<dl>
						<dt>邮政编码：</dt>
						<dd>
							<input type="text" id="yzbm" name="yzbm" placeholder="邮政编码" tilte="邮政编码">
						</dd>
					</dl>
					<dl>
						<dt>联系电话：</dt>
						<dd>
							<input type="text" id="lxdh" name="lxdh" placeholder="联系电话" title="联系电话">
						</dd>
					</dl>
					<dl>
						<dt>手机：</dt>
						<dd>
							<input type="text" id="sj" name="sj" placeholder="手机" title="手机">
						</dd>
					</dl>
					<dl>
						<dt>email：</dt>
						<dd>
							<input type="text" id="email" name="email" placeholder="email" title="电子邮箱">
						</dd>
					</dl>
				</div>
			</div>
			<div class="jb_middle">
				<img src="http://www.bjmtg.gov.cn/images/jb_jiantou20160421.gif">
			</div>
			<div class="jb_right">
				<div class="jb_title_r">被举报单位信息</div>
				<div class="jb_list">
					<dl>
						<dt>
							<span style="color: red; line-height: 30px;">*</span>单位名称：
						</dt>
						<dd>
							<input type="text" id="dwmc_bjb" name="dwmc_bjb" placeholder="被举报单位名称" title="被举报单位名称">
						</dd>
					</dl>
					<dl>
						<dt>
							<span style="color: red; line-height: 30px;">*</span>主要问题：
						</dt>
						<dd>
							<textarea name="zywt_bjb" id="zywt_bjb" class="txt2" placeholder="被举报单位主要问题" title="被举报单位主要问题"></textarea>
						</dd>
					</dl>
					<dl>
						<dt>
							<span style="color: red; line-height: 30px;">*</span>验证码：
						</dt>
						<dd>
							 <input name="certicode" id="certicode" title="请输入验证码" placeholder="验证码" type="text" onfocus="esdonfocus();"/>&nbsp; 
											<img id="verifyImage" title="验证码" alt="验证码"/>
											<a href="javascript:esdonfocus();" title="播放验证码"><img id="audio" src="../etc/image/volume2.png" alt="播放验证码" title="播放验证码" /></a>
											<input type="hidden" id="voicepath" /> 
						</dd>
					</dl>
				</div>
			</div>
			<div class="clear"></div>
			<div class="sub">
				<input id="SubmitButton" type="button" name="SubmitButton" onclick="jbdwSubmit()" value="提交" >&nbsp;&nbsp;<input type="button" value="重写" onclick="cx()" name="ResetButton" id="ResetButton">
			</div>
		</div>
	</div>
	<div id="footer"></div>
</body>
</html>
