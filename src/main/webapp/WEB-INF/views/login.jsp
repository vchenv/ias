<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/CryptoJS/aes.js"></script>
<script type="text/javascript" src="${contextPath}/js/CryptoJS/mode-ecb-min.js"></script>
<script type="text/javascript" src="${contextPath}/js/CryptoJS/pad-nopadding-min.js"></script>
<!-- <script type="text/javascript" src="${contextPath}/js?name='jquery-1.11.1.min.js'"></script>-->

<link rel="stylesheet" type="text/css" href="${contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${contextPath}/css/login.css" />
<title>无障碍云后台管理系统</title>
<script type="text/javascript">
	function login() {
		var username = $.trim($("#username").val());
		var password = $.trim($("#password").val());
		var code = $.trim($("#code").val());
		if (username == "" || username == null || password == ""
				|| password == null || code == null || code == "") {
			$("#str").html("用户信息或验证码不能为空!");
		} else {
			$("#login_form").submit();
		}
	}

	/* function changImg(obj) {
		obj
				.attr("src", "${contextPath }/random/code?t="
						+ new Date().getTime());
	}

	// 回车监听
	document.onkeydown = function(event) {
		var e = event || window.event || arguments.callee.caller.arguments[0];
		if (e && e.keyCode == 13) { // enter 键
			login();
		}
	};
	//aes加密
	function aesEncrypt(data, key) {
		var encrypted = CryptoJS.AES.encrypt(data, key, {
			mode : CryptoJS.mode.ECB,
			padding : CryptoJS.pad.Pkcs7
		});
		return encrypted.toString();
	}
	//aes解密
	function aesDecrypt(encrypted, key) {

		var decrypted = CryptoJS.AES.decrypt(encrypted, key, {
			mode : CryptoJS.mode.ECB,
			padding : CryptoJS.pad.Pkcs7
		});
		decrypted = CryptoJS.enc.Utf8.stringify(decrypted);// 转换为 utf8 字符串
		return decrypted;
	}
	function print(string) {
		document.write(string + "<br>");
	}
	var key = "1234567890654321";//16位
	key = CryptoJS.enc.Utf8.parse(key);
	var encrypted = aesEncrypt("Test String么么哒", key);
	var decrypt = aesDecrypt(encrypted.toString(), key);
	print("aes ecb 加密 解密");
	print("加密的密文:".concat(encrypted));
	print("解密密文的结果:" + decrypt); */
</script>
</head>
<body>
	<div class="page-container">
		<div class="main_box">
			<div class="login_box">
				<div class="login_logo">
					<h3><br>
						</h3><h3><b>无障碍云后台管理系统</b>
					</h3>
				</div>
				<div class="login_form">
					<form action="${contextPath }/iac/login" id="login_form" method="post">
						<div class="form-group">
							<label for="j_username" class="t">用 户：</label> <input id="username" name="username" type="text" class="form-control x319 in">
						</div>
						<div class="form-group">
							<label for="j_password" class="t">密 码：</label> <input id="password" name="password" type="password" class="password form-control x319 in">
						</div>
						<div class="form-group">
							<label for="j_captcha" class="t">验证码：</label> <input id="code" name="code" type="text" class="form-control x164 in" maxlength="4"> <img id="captcha_img" alt="点击更换" title="点击更换"
								src="${contextPath}/iac/random/code" class="m" onclick="changImg($(this))">
						</div>
						<div class="form-group">
							<label class="t"></label> <label for="j_remember" class="m" id="str" style="color: red;"> ${sessionScope.message} </label>
						</div>
						<div class="form-group space">
							<label class="t"></label>
							<button type="button" id="submit_btn" class="btn btn-primary btn-lg" onclick="login()">&nbsp;登&nbsp;录&nbsp;</button>
							<input type="reset" value="&nbsp;重&nbsp;置&nbsp;" class="btn btn-default btn-lg">
						</div>
					</form>
				</div>
			</div>
			<div class="bottom">
				Copyright &copy; 2014 - 2015 <a href="#">系统登陆</a>
			</div>
		</div>
		<%
			session.removeAttribute("message");
		%>
	</div>
	<!-- <div class="">
		<label >测试：</label> 
		<input id="button1" name="button1" type="button" class=""  value="我是按钮5"> 
		<input id="button2" name="button2" type="button" class="" >  
		<input id="testaes" name="testaes" type="text" class="">
	</div> -->
	<script type="text/javascript">
	/* $("#button1").click(function(){
		var key = "1234567890654321";
		key = CryptoJS.enc.Utf8.parse(key);
		var testaes = $("#testaes").val();
		var enc = aesEncrypt(testaes,key);
		alert(testaes+"加密了："+ enc);
//		$(this).children().children().children().find("input[type='radio']").attr("checked", "true");
		$.ajax({
			url : '${contextPath }/aes',
			type : 'POST',
			sync : true,
			data : {
				'testaes' : enc
			},
			success : function(data) {
				alert("来自后台的秘密："+data);
				var decrypt = aesDecrypt(data, key);
				alert("来自后台的秘密解密了："+decrypt);
			},
			error : function() {
			}
		});
	}); */
	</script>
</body>
</html>