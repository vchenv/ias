<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.esd.config.BaseConfig"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="Access-Control-Allow-Origin" content="*">

<link rel="stylesheet" type="text/css" href="${contextPath }/js/jquery-easyui-1.4.5/themes/default/easyui.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="${contextPath }/js/jquery-easyui-1.4.5/themes/icon.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="${contextPath }/css/core.css" charset="UTF-8" />
<script type="text/javascript" src="${contextPath }/js/public.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/jquery-easyui-1.4.5/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/jquery-easyui-1.4.5/jquery.easyui.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/ajaxfileupload.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/jquery.sortable.js" charset="UTF-8"></script>
 
<script type="text/javascript" src="${contextPath }/js/init.js" charset="UTF-8"></script>

<script type="text/javascript" src="${contextPath }/js/pgFile.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/template.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/css.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/js.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/cating.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/My97DatePicker/WdatePicker.js" charset="UTF-8"></script>

<title>无障碍云后台管理</title>
</head>
<body class="easyui-layout">
	<div region="north" split="true" style="height:60px;">
		<div id="progressbarAll" class="easyui-progressbar" style="height: 10px;"></div>
		<a href="javascript:catingAll();" class="easyui-linkbutton" iconCls="icon-start">开启采集</a> <a href="javascript:cancelCating();" class="easyui-linkbutton" iconCls="icon-cancel">取消采集</a> <input
			id="domain" value="<%=request.getSession().getAttribute(BaseConfig.DOMAINNAME)%>" class="easyui-textbox" style="width: 15%;"> <a href="javascript:cating();" class="easyui-linkbutton">一级采集</a>
		预览网址:<input id="url" class="easyui-textbox" style="width: 25%;">
		<!-- 		<a href="javascript:view();" class="easyui-linkbutton" text="预览"></a> -->
		<a href="javascript:view();" class="easyui-linkbutton" text="预览"></a> 
		<input id="time" class="Wdate" onfocus="WdatePicker({dateFmt:'HH:mm:ss'})" readonly="readonly" value="<%=BaseConfig.time%>" />
		<a href="javascript:setTimes();" class="easyui-linkbutton" text="开启"></a> 
		<a href="javascript:cancelTime();" class="easyui-linkbutton" text="关闭"></a> 
		<a href="javascript:countTimes();" class="easyui-linkbutton" text="定时次数"></a>
		<a href="javascript:createSite();"
			class="easyui-linkbutton" text="发布站点"></a> <a href="javascript:insertFiles();" class="easyui-linkbutton" text="测试数据"></a> <input id="test" value="测试数据" /> <input id="dir" value="测试数据dir" /> <input
			id="siteName" value="测试数据siteName" />
		<!-- 		<form name="Form2" action="${contextPath}/admin/uploadFiles" method="post"  enctype="multipart/form-data"> -->
		<!-- 			<input id="uploadFiles" type = "file"  name="uploadFiles"  style="width:100px" /> -->
		<!-- 			<input type="submit" value="上传"/> -->
		<!-- 		</form> -->
		<a href="javascript:window.location.href = '${contextPath}/record/record';" class="easyui-linkbutton" text="记录" style="float: right;"></a> <a
			href="javascript:window.location.href = '${contextPath}/iac/logout';" class="easyui-linkbutton" text="退出" style="float: left;"></a>
		<div id="progressbar" class="easyui-progressbar" style="height:10px;"></div>
	</div>
	<div id="pgFileList" region="east" title="规则列表" split="true" style="width:180px;padding: 10px;"></div>
	<div region="west" title="主编辑区" split="true" style="width:270px;">
		<div class="easyui-accordion" fit="true">
			<div title="规则编辑区" selected="true" style="padding:5px;">
				<ul id="rules" class="gbin1-list">
				</ul>
			</div>
			<div title="模板列表" id="templateFileList"></div>
			<div title="样式列表" id="cssFileList"></div>
			<div title="脚本列表" id="jsFileList"></div>
		</div>
	</div>
	<div region="center" border="false">
		<div class="easyui-layout" fit="true">
			<div region="north" border="false" split="true" title="采集网址集合" style="height: 30%;">
				<div class="dourls">
					规则名称: <input id="pgFileName" class="easyui-textbox" /> <a href="javascript:savePgFile();" class="easyui-linkbutton" iconCls="icon-save">保存</a> <a href="javascript:deletePgFile();"
						class="easyui-linkbutton" iconCls="icon-clear">删除</a> &nbsp;&nbsp;开启脚本: <select id="javaScriptEnabled" class="easyui-combobox" panelHeight="auto" editable="false">
						<option value="false">禁用</option>
						<option value="true">启用</option>
					</select> 异步加载： <input id="sleep" class="easyui-textbox" value="0" style="width: 50px;" />
				</div>
				<textarea id="urls"></textarea>
			</div>
			<div region="center" split="true" border="false">
				<div id="tabs" class="easyui-tabs" border="false" fit="true">
					<div title="模板">
						<div class="dofile">
							文件名称: <input id="templateName" class="easyui-textbox" /> <a href="javascript:saveTemplate();" class="easyui-linkbutton" iconCls="icon-save">保存</a> <a href="javascript:deleteTemplate();"
								class="easyui-linkbutton" iconCls="icon-clear">删除</a> &nbsp;&nbsp;&nbsp;&nbsp; <input id="myBlogImage" class="easyui-filebox" name="myfiles"
								data-options="prompt:'选择图片文件',buttonText:'&nbsp;选&nbsp;择&nbsp;'" style="width:170px" /> <a href="javascript:ajaxFileUpload();" class="easyui-linkbutton">上传图片</a>
						</div>
						<textarea id="template_content"></textarea>
					</div>
					<div title="样式">
						<div class="dofile">
							文件名称: <input id="cssName" class="easyui-textbox" /> <a href="javascript:saveCss();" class="easyui-linkbutton" iconCls="icon-save">保存</a> <a href="javascript:deleteCss();"
								class="easyui-linkbutton" iconCls="icon-clear">删除</a>
						</div>
						<textarea id="css_content"></textarea>
					</div>
					<div title="脚本">
						<div class="dofile">
							文件名称: <input id="jsName" class="easyui-textbox" /> <a href="javascript:saveJs();" class="easyui-linkbutton" iconCls="icon-save">保存</a> <a href="javascript:deleteJs();" class="easyui-linkbutton"
								iconCls="icon-clear">删除</a>
						</div>
						<textarea id="js_content"></textarea>
					</div>
					<!--   <div title="正页">
				    	<div class="dofile">
				    		文件名称:
				    		<input id="htmlName" class="easyui-textbox"/>
				    		<a href="javascript:getHtml();" class="easyui-linkbutton" iconCls="icon-save">查询</a>
				    		<a href="javascript:saveHtml();" class="easyui-linkbutton" iconCls="icon-save">保存</a>
				    		<a href="javascript:deleteHtml();" class="easyui-linkbutton" iconCls="icon-clear">删除</a>
				    	</div>
				        <textarea id="js_content"></textarea>
				    </div>
				</div> -->
				</div>
			</div>
		</div>
</body>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/lib/codemirror.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/selection/active-line.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/edit/matchtags.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/edit/matchbrackets.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/edit/closetag.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/edit/closebrackets.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/search/searchcursor.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/search/search.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/fold/xml-fold.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/hint/show-hint.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/hint/xml-hint.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/hint/html-hint.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/hint/javascript-hint.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/addon/display/fullscreen.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/keymap/sublime.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/mode/xml/xml.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/mode/css/css.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/mode/javascript/javascript.js" charset="UTF-8"></script>
<script type="text/javascript" src="${contextPath }/js/codemirror-5.18.2/mode/htmlmixed/htmlmixed.js" charset="UTF-8"></script>

<link rel="stylesheet" type="text/css" href="${contextPath }/js/codemirror-5.18.2/lib/codemirror.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="${contextPath }/js/codemirror-5.18.2/doc/docs.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="${contextPath }/js/codemirror-5.18.2/addon/hint/show-hint.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="${contextPath }/js/codemirror-5.18.2/addon/display/fullscreen.css" charset="UTF-8" />
<style type="text/css">
.CodeMirror {
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px;
	border: none;
	font-size: 14px;
}
</style>
<script>
var urls_content = document.getElementById("urls");
var urlsTextArea = CodeMirror.fromTextArea(urls_content, {
	keyMap : "sublime",
	styleActiveLine : true,
	lineNumbers : true
});
var template_content = document.getElementById("template_content");
var templateEditor = CodeMirror.fromTextArea(template_content, {
	mode : "text/html",
	keyMap : "sublime",
	matchTags : {
		bothTags : true
	},
	extraKeys : {
		"Ctrl-J" : "toMatchingTag",
		"Ctrl-M" : "autocomplete",
		"F11" : function(cm) {
			cm.setOption("fullScreen", !cm.getOption("fullScreen"));
		},
		"Esc" : function(cm) {
			if (cm.getOption("fullScreen"))
				cm.setOption("fullScreen", false);
		}
	},
	styleActiveLine : true,
	lineNumbers : true,
	autoCloseTags : true,
	autoCloseBrackets : true,
	matchBrackets : true,
	gutters : [ "CodeMirror-lint-markers" ]
});

var css_content = document.getElementById("css_content");
var cssEditor = CodeMirror.fromTextArea(css_content, {
	mode : "text/css",
	keyMap : "sublime",
	matchTags : {
		bothTags : true
	},
	extraKeys : {
		"Ctrl-J" : "toMatchingTag",
		"Ctrl-M" : "autocomplete",
		"F11" : function(cm) {
			cm.setOption("fullScreen", !cm.getOption("fullScreen"));
		},
		"Esc" : function(cm) {
			if (cm.getOption("fullScreen"))
				cm.setOption("fullScreen", false);
		}
	},
	styleActiveLine : true,
	lineNumbers : true,
	autoCloseTags : true,
	autoCloseBrackets : true,
	matchBrackets : true,
	gutters : [ "CodeMirror-lint-markers" ],
});

var js_content = document.getElementById("js_content");
var jsEditor = CodeMirror.fromTextArea(js_content, {
	mode : "text/javascript",
	keyMap : "sublime",
	matchTags : {
		bothTags : true
	},
	extraKeys : {
		"Ctrl-J" : "toMatchingTag",
		"Ctrl-M" : "autocomplete",
		"F11" : function(cm) {
			cm.setOption("fullScreen", !cm.getOption("fullScreen"));
		},
		"Esc" : function(cm) {
			if (cm.getOption("fullScreen"))
				cm.setOption("fullScreen", false);
		}
	},
	styleActiveLine : true,
	lineNumbers : true,
	autoCloseTags : true,
	autoCloseBrackets : true,
	matchBrackets : true,
	gutters : [ "CodeMirror-lint-markers" ],
});
/*******************************************************************************/
createSite = function() {
	$.ajax({
		type : 'POST',
		url : 'createSite',
		dataType : 'json',
		data : {
			type : 0
		},
		success : function(data) {
			alert("回话：" + data);
		}
	});
};

insertFiles = function() {
	var fileType = $("#test").val();
	var dir = $("#dir").val();
	var siteName = $("#siteName").val();
	alert(fileType);
	$.ajax({
		type : 'POST',
		url : '/dbFiles/insertFiles',
		dataType : 'json',
		data : {
			fileType : fileType,
			url : dir,
			siteName : siteName
		},
		success : function(data) {
			alert("回话：" + data);
		}
	});
};
view1 = function() {
	var url = $("#url").val();
	var rules = getRules();
	var sleep = $("#sleep").val();
	var javaScriptEnabled = $("#javaScriptEnabled").combobox('getValue');
	var template = $("#templateName").val();
	if (url == "" || url.length == 0) {
		alert("请填写预览的域名！");
	} else {
		window.open('/iac/view?url=' + url + '&&rules=' + rules
				+ '&&sleep=' + sleep + '&&javaScriptEnabled='
				+ javaScriptEnabled + '&&template=' + template);

	}
};
countTimes = function() {

	$.ajax({
		url : root + '/countTimes',
		type : 'POST',
		sync : false,
		data : {

		},
		success : function(data) {
			$.messager.alert('提示', data.countTimes, 'info');
		},
		error : function() {
			$.messager.alert('发生错误', '定时任务设置失败!', 'error');
		}
	});
}
</script>
</html>