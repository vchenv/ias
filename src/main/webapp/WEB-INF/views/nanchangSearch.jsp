<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/nanchangSearch.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=index.css" />
<title>关键字搜索</title>
<script type="text/javascript">

	function search(currentPage){
		var keyWord = $("#keyWord").val();
		if(keyWord == ""){
			alert("请输入您想要搜索的关键字！")
			return;
		}
		var pattern = new RegExp("[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]");
		for (var i = 0; i < keyWord.length; i++) {
			if(pattern.test(keyWord.substr(i,1))){
				alert("搜索词中请不要输入特殊符号！");
			    return ;
			}
		}
		
		window.location.href="nanchangSearch?keyWord="+encodeURI(encodeURI(keyWord))+"&currentPage="+currentPage;
		
	}

	$(function(){
		$("#keyWord").focus();
		
		$.ajax({
			url:'showTemplate?fileName=header.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#header").append(data);
			}
		});
		
		$.ajax({
			url:'showTemplate?fileName=footer.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#footer").append(data);
			}
		});
	})
	
	function goPage(currentPage) {
			var hidserCon = $("#keyWord").val();
			$("#serCon").val(hidserCon);
			$("#currentPage").val(currentPage);
			search(currentPage);
		}
</script>
</head>
<body>
	<div id = "header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>关键字搜索</h2>
				<div class="keyWord">关键字：
					<input type="text" id="keyWord" title="请输入关键字"  value="${keyWord }"/>
					<button  id="searchButton" title="搜索" onclick="search(1)">搜   索</button>
				</div>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="Sbmenu">
					<span> 查询结果： </span>检索到记录${html.total}条
				</div>
				<c:forEach items="${list }" var="list"  varStatus="i">
				<div class="xx_inputs">
					<a href="${list.id }"  title="${list.title }">${list.title }</a><div><span title="${list.updateDate }">${list.updateDate }</span></div>
				</div>
				</c:forEach>
			<div class="xx_inputs"></div>
			
			<div class="page" style="margin-left: 10px; padding-bottom: 30px; width: 1000px; border: 0;">
			<nobr>
				<c:if test="${html.currentPage>1}">
					<a class="first-page" href="javascript:goPage('1');" target="_self">首页</a>
					<a class="prev-page" href="javascript:goPage(${html.currentPage - 1});" target="_self">上一页</a>
				</c:if>
				<c:if test="${html.totalPage<=11}">
					<c:set var="begin" value="1"></c:set>
					<c:set var="end" value="${html.totalPage}"></c:set>
				</c:if>
				<c:if test="${html.totalPage>11}">
					<c:choose>
						<c:when test="${html.currentPage<=5}">
							<c:set var="begin" value="1"></c:set>
							<c:set var="end" value="11"></c:set>
						</c:when>
						<c:when test="${html.currentPage>=html.totalPage-5}">
							<c:set var="begin" value="${html.totalPage-11}"></c:set>
							<c:set var="end" value="${html.totalPage}"></c:set>
						</c:when>
						<c:otherwise>
							<c:set var="begin" value="${html.currentPage-5}"></c:set>
							<c:set var="end" value="${html.currentPage+5}"></c:set>
						</c:otherwise>
					</c:choose>
				</c:if>
				<c:forEach begin="${begin}" end="${end}" var="i">
					<c:choose>
						<c:when test="${html.currentPage==i}">
							<span class="current-page"> ${i} </span>
						</c:when>
						<c:otherwise>
							<a target="_self" href="javascript:goPage('${i }');"> ${i } </a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:if test="${html.currentPage<html.totalPage}">
					<a class="next-page" href="javascript:goPage('${html.currentPage + 1}');" target="_self"> 下一页 </a>
					<a class="last-page" href="javascript:goPage('${html.totalPage}');" target="_self"> 尾页 </a>
				</c:if>
			</nobr>
			&nbsp;&nbsp;&nbsp;&nbsp; 共有${html.total}条记录 当前页数:${html.currentPage }/总页数:${html.totalPage}<br>
		</div>
			</div>
		</div>
	</div>
	<div id ="footer"></div>
</body>
</html>
