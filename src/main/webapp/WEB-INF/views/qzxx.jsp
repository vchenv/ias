<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/public.js"></script>
<script type="text/javascript" src="${contextPath}/etc/tool/Voice.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/xx.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=index.css" />
<title>区长信箱</title>
<script type="text/javascript">
$.ajaxSetup({
	cache : false
});
$(document).ready(function() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
	$("#verifyImage").load(function() {
		var timestamp = new Date().getTime();
		$.ajax({
			type : 'get',
			url : '../iac/random/getCode?' + timestamp,
			dataType : 'json',
			success : function(data) {
				$("#voicepath").val(data.path);
				// alert(data.path);
				// return data;
			}
		});
	});
	$("#verifyImage").click(function() {
		var timestamp = new Date().getTime();
		$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);

	});
});
function changeImage() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
}

$(function(){
	var mark = "qzxx";
	
	$.ajax({
		url:'xxUrl',
		type:'GET',
		sync: true,
		data:{
			"mark":mark
		},
		success:function(){
			
		}
	});
})
function goSubmit(){
	var name1 = document.getElementById("name");
	if(!name1.value){
		alert('姓名不能为空');
		return false;
		}
		
	var pattern = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]");
	for (var i = 0; i < name1.value.length; i++) {
		if(pattern.test(name1.value.substr(i,1))){
			alert("姓名中请不要输入特殊符号");
		    return false;
		}
	}
	
	var namelength = document.getElementById("name").value.length;
	if(namelength>=50){
		alert('输入内容不能超出25个汉字');
		return false;
	}

    var tel1= document.getElementById("tel");
	if(!tel1.value){
		alert('电话不能为空');
		return false;
		}
	
	for (var i = 0; i < tel1.value.length; i++) {
		if(pattern.test(tel1.value.substr(i,1))){
			alert("电话中请不要输入特殊符号");
		    return false;
		}
	}

    var dzyx1= document.getElementById("email");
	if(!dzyx1.value){
		alert("电子邮箱不能为空");
		return false;
	}else{
		if(!/^[0-9a-z][a-z0-9._-]{1,}@[a-z0-9-]{1,}[a-z0-9].[a-z.]{1,}[a-z]$/.test(dzyx1.value)){
			alert("电子邮箱格式不正确");
			return false;
		}
	}

	for (var i = 0; i < dzyx1.value.length; i++) {
		if(pattern.test(dzyx1.value.substr(i,1))){
			alert("电子邮箱中请不要输入特殊符号");
		    return false;
		}
	}
    var title1= document.getElementById("title");
	if(!title1.value){
		alert('标题不能为空');
		return false;
		}
		
	for (var i = 0; i < title1.value.length; i++) {
		if(pattern.test(title1.value.substr(i,1))){
			alert("标题中请不要输入特殊符号");
		    return false;
		}
	}
	
	var con= document.getElementById("con");
	var conlength = con.value.length;
	if(conlength >=2000){
		alert('输入内容不能超出2000个汉字');
		return false;
	}
	for (var i = 0; i < con.value.length; i++) {
		if(pattern.test(con.value.substr(i,1))){
			alert("输入内容中请不要输入特殊符号");
		    return false;
		}
	}
	
	var certicode= document.getElementById("certicode");
	var conlength = certicode.value.length;
	for (var i = 0; i < certicode.value.length; i++) {
		if(pattern.test(certicode.value.substr(i,1))){
			alert("输入验证码中请不要输入特殊符号");
		    return false;
		}
	}
	$.ajax({
		url:'xxsubmit',
		type:'POST',
		sync: true,
		data:{
			"name":name1.value,
			"tel":tel1.value,
			"email":dzyx1.value,
			"title":title1.value,
			"content":con.value,
			"certicode":certicode.value
		},
		success:function(data){
			if(data == 0){
				$(".xx_wrap").attr("style","display:none;")
				$(".container").append("</br></br></br>"+
						"<img src='http://www.bjmtg.gov.cn/infogate/customer/images/icon_alert_error.gif' border='0' alt='表单提交失败'></br></br>"+
						"<span style='font-size: 20px; color: red;'><b>表单提交失败</b></span></br></br></br>"+
						"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
			}
			if(data == 1){
				$(".xx_wrap").attr("style","display:none;")
				$(".container").append("</br></br></br>"+
						"<img src='http://www.bjmtg.gov.cn/infogate/customer/images/icon_alert_success.gif' border='0' alt='表单提交成功'></br></br>"+
						"<span style='font-size: 20px; color: red;'><b>表单提交成功</b></span></br></br></br>"+
						"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
			}
			if(data == 2){
				$(".xx_wrap").attr("style","display:none;")
				$(".container").append("</br></br></br>"+
						"<img src='http://www.bjmtg.gov.cn/infogate/customer/images/icon_alert_error.gif' border='0' alt='表单提交失败'></br></br>"+
						"<span style='font-size: 20px; color: red;'><b>表单提交失败</b></span></br></br></br>"+
						"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
			}
		}
	});
}

	function sx(){
		location.reload();
	}
	function qk(){
		document.getElementById("name").value="";
		document.getElementById("email").value="";
		document.getElementById("tel").value="";
		document.getElementById("title").value="";
		document.getElementById("con").value="";
		document.getElementById("certicode").value="";
	}
	
	$(function(){
		$.ajax({
			url:'showTemplate?fileName=header.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#header").append(data);
			}
		});
		
		$.ajax({
			url:'showTemplate?fileName=footer.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#footer").append(data);
			}
		});
		
	})
</script>
</head>
<body>
	<div id = "header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>区长信箱</h2>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="xx_inputs">
					<label for="name">姓&nbsp;&nbsp;&nbsp;&nbsp;名：</label> <input type="text" id="name" name="name"  placeholder="姓名" title="姓名"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="email">电子邮箱：</label> <input type="text" id="email" name="email" placeholder="电子邮箱" title="电子邮箱"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="tel">联系电话：</label> <input type="text" id="tel" name="tel" placeholder="联系电话" title="联系电话"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="title">标&nbsp;&nbsp;&nbsp;&nbsp;题：</label> <input type="text" id="title" name="title" placeholder="标题" title="标题"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="con">内&nbsp;&nbsp;&nbsp;&nbsp;容：</label> <textarea type="text" id="con" name="con" placeholder="内容" title="内容"></textarea> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="certicode">验&nbsp;证&nbsp;码：</label> <input name="certicode" id="certicode" title="请输入验证码" type="text" onfocus="esdonfocus();"/>&nbsp; 
											<img id="verifyImage" title="验证码" alt="验证码"/>
											<a href="javascript:esdonfocus();" title="播放验证码"><img id="audio" src="../etc/image/volume2.png" alt="播放验证码" title="播放验证码" /></a>
											<input type="hidden" id="voicepath" /> <span>*</span>
				</div>
				<div class="xx_btns">
					<button type="button" class="upd_pwd_btn" onclick="goSubmit()" title="提交">提交</button>
					<button type="button" class="clean_pwd_btn" onclick="qk()" title="重置">重置</button>
				</div>
			</div>
		</div>
	</div>
	<div id = "footer"></div>
</body>
</html>
