<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>搜索结果页面</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="Record Page">
<meta content="width=device-width, initial-scale=1" name="viewport">
<script type="text/javascript" src="/js/jquery-easyui-1.4.5/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/jquery-easyui-1.4.5/jquery.easyui.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/jquery-easyui-1.4.5/easyloader.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/jquery-easyui-1.4.5/locale/easyui-lang-zh_CN.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="/js/jquery-easyui-1.4.5/themes/default/easyui.css" charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/js/jquery-easyui-1.4.5/themes/icon.css" charset="UTF-8" />



<style type="text/css">
body {
	font-family: verdana, helvetica, arial, sans-serif;
	padding: 20px;
	font-size: 12px;
	margin: 0;
}

h2 {
	font-size: 18px;
	font-weight: bold;
	margin: 0;
	margin-bottom: 15px;
}

.demo-info {
	padding: 0 0 12px 0;
}

.demo-tip {
	display: none;
}

.label-top {
	display: block;
	height: 22px;
	line-height: 22px;
	vertical-align: middle;
}

#result_list tr {
	width: 98%;
}
</style>
<script type="text/javascript">
	
</script>

</head>
<body>

	<%
		String domain = request.getScheme() + "://" + request.getServerName();
	%>
	<div class="input_btn">
		<form action="${contextPath}/getRecord" method="post" id="search" name="search" role="form" class="form-signin">
			<input type="text" id="serCon" name="serCon" value="${result.serCon}" class="form-control" value="${serCon}">
			<button id="search-btn" type="button" class="btn btn-primary">搜索</button>
			<br> <input id="currentPage" type="hidden" name="currentPage" value="${result.currentPage}"> <input id="hidserCon" type="hidden" value="${result.serCon}">
		</form>
	</div>
	<div class="content">
		<p id="totle"></p>
		<table id="mylist">
			<thead>
				<tr>
					<!--  <th data-options="field:'leve',width:100,align:'center'">序号</th> -->
					<th data-options="field:'leve',width:100,align:'center'">等级</th>
					<th data-options="field:'url',width:200,align:'center'">url</th>
					<th data-options="field:'Md5url',width:200,align:'center'">Md5url</th>
					<th data-options="field:'result',width:100,align:'center'">处理状态</th>
					<th data-options="field:'type',width:100,align:'center'">类型</th>
				</tr>
			</thead>
			<!-- <tbody id="record-tbody"></tbody> -->
		</table>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#mylist').datagrid({
				title : 'datagrid实例',
				iconCls : 'icon-ok',
				width : 600,
				pageSize : 5,//默认选择的分页是每页5行数据  
				pageList : [ 5, 10, 15, 20 ],//可以选择的分页集合  
				nowrap : true,//设置为true，当数据长度超出列宽时将会自动截取  
				striped : true,//设置为true将交替显示行背景。  
				collapsible : true,//显示可折叠按钮  
				toolbar : "#tb",//在添加 增添、删除、修改操作的按钮要用到这个  
				url : '/iac/record/getRecord',//url调用Action方法  
				loadMsg : '数据装载中......',
				singleSelect : true,//为true时只能选择单行  
				fitColumns : true,//允许表格自动缩放，以适应父容器  
				//sortName : 'xh',//当数据表格初始化时以哪一列来排序  
				//sortOrder : 'desc',//定义排序顺序，可以是'asc'或者'desc'（正序或者倒序）。  
				remoteSort : false,
				frozenColumns : [ [ {
					field : 'ck',
					checkbox : true
				} ] ],
				pagination : true,//分页  
				rownumbers : true
			//行数  
			});

		});
		/* $(document).ready(function() {
			$.ajax({
				type:'POST',
				
				url:'${contextPath}/record/getRecord', 
				dataType:'json',
				success:function(data){
				
						pageTotal = data.totle;
						$("#totle").append("检索到记录:"+data.totle+"条"); 
						$.each(data.list,function(i,item){
						if(i<10){
						//"<th field='' width='100'>"+(i + 1)+"</th>"+
							$("#record-tbody").append(
							"<tr>"+
								
								"<th field='leve' width='100'>"+item.leve+"</th>"+
								"<th field='url' width='200'>"+item.url+"</th>"+
								"<th field='md5url' width='200'>"+item.md5url  +"</th>"+
								"<th field='result' width='200'>"+item.result+"</th>"+
								"<th field='type' width='100'>"+item.type+"</th>"+
							"</tr>"
							);
						}		
						});
					}
			});
		
		}); */

		/* var pager = $('#tt').datagrid('getPager');    // get the pager of datagrid
		pager.pagination({
		showPageList:false,
		buttons:[{
			iconCls:'icon-search',
			handler:function(){
				alert('search');
			}
		},{
			iconCls:'icon-add',
			handler:function(){
				alert('add');
			}
		},{
			iconCls:'icon-edit',
			handler:function(){
				alert('edit');
			}
		}],
		onBeforeRefresh:function(){
			alert('before refresh');
			return true;
		}
		}); */
	</script>
</body>
</html>
