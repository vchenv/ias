<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/zxqz.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=index.css" />
<title>网上咨询</title>
<script type="text/javascript">

function gotoWyly(){
	window.location.href="gotoWyly";
}
	
</script>
</head>
<body>
	<div id = "header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>网上咨询</h2>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="wxts">
					&nbsp;&nbsp;&nbsp;&nbsp;当您留言成功后，系统会提供一个反馈号用来查询。
					<button type="button" class="upd_pwd_btn" onclick="gotoWyly()" title="留言">留言</button>
					</br></br>
		            &nbsp;&nbsp;&nbsp;&nbsp;查看我的信件 (在此输入您的反馈号):
		            <input type="text" id="NO" name="NO"  placeholder="" title="反馈号"/> 
		            <button type="button" class="upd_pwd_btn" onclick="gotoWyly()" title="查看">查看</button>
					</br></br>
				</div>
			</div>	
		</div>
	</div>
	<div id ="footer"></div>
</body>
</html>
