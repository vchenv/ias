<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/zxqz.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=index.css" />
<title>我要留言</title>
<script type="text/javascript">
$(function(){
	var url = "http://spjc.bjmtg.gov.cn/hrapp/webanser/savebody.jsp";
	$.ajax({
		url:'wyly',
		type:'POST',
		sync: true,
		data:{
			"url":url
		},
		success:function(){
			
		}
	});
})
	function goSubmit(){
		
		var pattern = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]");
		
		var Name = $("#Name").val();
		if(Name == ""){
			alert('姓名不能为空');
			return false;
			}
		
		for (var i = 0; i < Name.length; i++) {
			if(pattern.test(Name.substr(i,1))){
				alert("姓名中请不要输入特殊符号");
			    return false;
			}
		}
		
		
        var email= $("#email").val();
		if(email == ""){
			alert('电子邮箱不能为空');
			return false;
			}
		for (var i = 0; i < email.length; i++) {
			if(pattern.test(email.substr(i,1))){
				alert("电子邮箱中请不要输入特殊符号");
			    return false;
			}
		}
        var Phone= $("#Phone").val();
		for (var i = 0; i < Phone.length; i++) {
			if(pattern.test(Phone.substr(i,1))){
				alert("电话中请不要输入特殊符号");
			    return false;
			}
		}
		
		var title = $("#title").val();
		if(title == ""){
			alert('留言主题不能为空');
			return false;
			}
		for (var i = 0; i < title.length; i++) {
			if(pattern.test(title.substr(i,1))){
				alert("留言主题中请不要输入特殊符号");
			    return false;
			}
		}
		
		var lybm = $("#lybm").val();
		
		var Con= $("#Con").val();
		if(Con == ""){
			alert('内容不能为空');
			return false;
			}
		for (var i = 0; i < Con.length; i++) {
			if(pattern.test(Con.substr(i,1))){
				alert("内容中请不要输入特殊符号");
			    return false;
			}
		}
		$.ajax({
			url:'wylySubmit',
			type:'POST',
			sync: true,
			data:{
				"Name":Name,
				"email":email,
				"Phone":Phone,
				"title":title,
				"lybm":lybm,
				"Con":Con
			},
			success:function(data){
				$(".xx_wrap").attr("style","display:none;")
				$(".container").append("</br></br></br>"+
						"您的留言成功，经审核后会在本页显示，您的反馈号码是：</br></br>"+
						"<span style='font-size: 20px; color: red;'><b>"+data+"</b></span></br></br></br>"+
						"<input type='button' value='返回' onclick='sx()'></br></br></br></br></br>");
			}
		});
	}

	function sx(){
		location.reload();
	}
	
	function qk(){
		document.getElementById("Name").value="";
		document.getElementById("Phone").value="";
		document.getElementById("Address").value="";
		document.getElementById("Con").value="";
	}
	
	$(function(){
		$.ajax({
			url:'showTemplate?fileName=header.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#header").append(data);
			}
		});
		
		$.ajax({
			url:'showTemplate?fileName=footer.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#footer").append(data);
			}
		});
		
	})
</script>
</head>
<body>
	<div id = "header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>我要留言</h2>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="wxts" style="text-align: center;">
					请您正确填写您的信息，以便我们与您联系（带有<span style="color: #ff0000" class="red">*</span>号的必填项）
					</br></br>
				</div>
				<div class="xx_inputs">
					<label for="Name">留&nbsp;言&nbsp;人：</label> <input type="text" id="Name" name="Name"  placeholder="留言人" title="留言人"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="Name">电子邮箱：</label> <input type="text" id="email" name="email"  placeholder="电子邮箱" title="电子邮箱"/> <span>*</span>
				</div>
				
				<div class="xx_inputs">
					<label for="tel">电&nbsp;&nbsp;&nbsp;&nbsp;话：</label> <input type="text" id="Phone" name="Phone" placeholder="电话" title="电话"/> <span>&nbsp;</span>
				</div>
				<div class="xx_inputs">
					<label for="title">留言主题：</label> <input type="text" id="title" name="title" placeholder="留言主题" title="留言主题"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="lybm">选择部门：</label> 
					<select id="lybm">
						<option value="">请选择部门</option>
						<option value="1000000286">区发改委</option>
						<option value="1000000149">区教委</option>
						<option value="1000000291">区住建委</option>
						<option value="1000000170">区商务委</option>
						<option value="1000000219">区文委</option>
						<option value="1000000178">区市政市容委</option>
						<option value="1000000189">区科委</option>
						<option value="1000000186">区经信委</option>
						<option value="1000000289">区旅游委</option>
						<option value="1000000226">区民政局</option>
						<option value="1000000290">区司法局</option>
						<option value="1000000006">区财政局</option>
						<option value="1000000153">区人保局</option>
						<option value="1000000241">区环保局</option>
						<option value="1000000285">区交通局</option>
						<option value="1000000292">区水务局</option>
						<option value="1000000287">区卫计委</option>
						<option value="1000000215">区安监局</option>
						<option value="1000000284">区体育局</option>
						<option value="1000000202">区统计局</option>
						<option value="1000000193">区农业局</option>
						<option value="1000000288">区园林绿化局</option>
						<option value="1000000223">区民防局</option>
						<option value="1000000184">区地震局</option>
						<option value="1000000469">区档案局</option>
						<option value="1000000470">区经管站</option>
						<option value="1000000246">大峪街道办</option>
						<option value="1000000247">城子街道办</option>
						<option value="1000000248">东辛房街道办</option>
						<option value="1000000249">大台街道办</option>
						<option value="1000000294">军庄镇</option>
						<option value="1000000295">龙泉镇</option>
						<option value="1000000296">妙峰山镇</option>
						<option value="1000000297">清水镇</option>
						<option value="1000000298">潭柘寺镇</option>
						<option value="1000000299">王平镇</option>
						<option value="1000000300">雁翅镇</option>
						<option value="1000000301">永定镇</option>
						<option value="1000000302">斋堂镇</option>
					</select> 
					<span>&nbsp;</span>
				</div>
				<div class="xx_inputs">
					<label for="con">留言内容：</label> <textarea type="text" id="Con" name="Con" placeholder="留言内容" title="留言内容"></textarea> <span>*</span>
				</div>
				<div class="xx_btns">
					<button type="button" class="upd_pwd_btn" onclick="goSubmit()" title="提交">提交</button>
					<button type="button" class="clean_pwd_btn" onclick="qk()" title="重置">重置</button>
				</div>
			</div>
		</div>
	</div>
	<div id ="footer"></div>
</body>
</html>
