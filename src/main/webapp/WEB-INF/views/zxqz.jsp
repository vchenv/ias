<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="public.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/public.js"></script>
<script type="text/javascript" src="${contextPath}/etc/tool/Voice.js"></script>
<link rel="stylesheet" type="text/css" href="${contextPath}/css/zxqz.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=public.css" />
<link rel="stylesheet" type="text/css" href="/iac/iac/css?name=index.css" />
<title>在线求助</title>
<script type="text/javascript">
$.ajaxSetup({
	cache : false
});
$(document).ready(function() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
	$("#verifyImage").load(function() {
		var timestamp = new Date().getTime();
		$.ajax({
			type : 'get',
			url : '../iac/random/getCode?' + timestamp,
			dataType : 'json',
			success : function(data) {
				$("#voicepath").val(data.path);
				// alert(data.path);
				// return data;
			}
		});
	});
	$("#verifyImage").click(function() {
		var timestamp = new Date().getTime();
		$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);

	});
});
function changeImage() {
	var timestamp = new Date().getTime();
	$("#verifyImage").attr("src", "../iac/random/code?" + timestamp);
}

$(function(){
	var url = "http://www.61696156.gov.cn/onlineHelp.aspx";
	$.ajax({
		url:'zxqz',
		type:'POST',
		sync: true,
		data:{
			"url":url
		},
		success:function(){
			
		}
	});
})
	function goSubmit(){
		
		var pattern = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]");
		
		var isNiMing = $("#isNiMing").val();
		
		var Name = $("#Name").val();
		if(Name == ""){
			alert('姓名不能为空');
			return false;
			}
		
		for (var i = 0; i < Name.length; i++) {
			if(pattern.test(Name.substr(i,1))){
				alert("姓名中请不要输入特殊符号");
			    return false;
			}
		}
		
		var Sex = $("#Sex").val();
		
        var Phone= $("#Phone").val();
		if(Phone == ""){
			alert('电话不能为空');
			return false;
			}
		for (var i = 0; i < Phone.length; i++) {
			if(pattern.test(Phone.substr(i,1))){
				alert("电话中请不要输入特殊符号");
			    return false;
			}
		}
		
		var Address = $("#Address").val();
		for (var i = 0; i < Address.length; i++) {
			if(pattern.test(Address.substr(i,1))){
				alert("地址中请不要输入特殊符号");
			    return false;
			}
		}
		
		var Con= $("#Con").val();
		if(Con == ""){
			alert('内容不能为空');
			return false;
			}
		for (var i = 0; i < Con.length; i++) {
			if(pattern.test(Con.substr(i,1))){
				alert("内容中请不要输入特殊符号");
			    return false;
			}
		}
		var certicode= $("#certicode").val();
		if(certicode == ""){
			alert('验证码不能为空');
			return false;
			}
		for (var i = 0; i < certicode.length; i++) {
			if(pattern.test(certicode.substr(i,1))){
				alert("验证码中请不要输入特殊符号");
			    return false;
			}
		}
		
		
		$.ajax({
			url:'zxqzSubmit',
			type:'POST',
			sync: true,
			data:{
				"isNiMing":isNiMing,
				"Name":Name,
				"Sex":Sex,
				"Phone":Phone,
				"Address":Address,
				"Con":Con,
				"certicode":certicode
			},
			success:function(data){
				if(data == 0){
					alert("添加失败！")
					location.reload();
				}
				if(data == 1){
					alert("添加成功！")
					location.reload();
				}
			}
		});
	}

	function qk(){
		document.getElementById("Name").value="";
		document.getElementById("Phone").value="";
		document.getElementById("Address").value="";
		document.getElementById("Con").value="";
		document.getElementById("certicode").value="";
	}
	
	$(function(){
		$.ajax({
			url:'showTemplate?fileName=header.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#header").append(data);
			}
		});
		
		$.ajax({
			url:'showTemplate?fileName=footer.html',
			type:'GET',
			sync: false,
			success:function(data){
				$("#footer").append(data);
			}
		});
		
	})
</script>
</head>
<body>
	<div id = "header"></div>
	<div class="container content">
		<div class="xx_wrap select_wrap">
			<div class="page_title">
				<h2>在线求助</h2>
				<div class="clearboth"></div>
			</div>
			<div class="xx" id="content">
				<div class="wxts">
					&nbsp;&nbsp;&nbsp;&nbsp;欢迎您使用“在线求助”，请将您的咨询、求助、投诉、建议、便利服务申请内容详细填写，我们将尽快为您办理并回复办理结果。求助、投诉、便利服务申请请您如实填写姓名、地址、联系方式等信息，以便我们的工作人员能够及时与您联系核实情况，我们会对您的个人信息进行保密。
					</br>
		            &nbsp;&nbsp;&nbsp;&nbsp;温馨提示：“实名”用户，能够凭账号登录查询到您个人求助事项的办理结果（其他用户不可查看，也不能查看其他实名用户的求助事项办理结果）和所有匿名用户求助事项的办理结果；您也可以游客（“匿名”用户）身份留言求助，匿名用户只能查看公开发布的匿名用户咨询问答事项。
					</br>
					备注：其中<span style="color: #ff0000" class="red">*</span>为必填。
					</br></br>
				</div>
				<div class="xx_inputs">
					<label for="isNiMing">是否匿名：</label> 
					<select id="isNiMing">
						<option value="1">匿名</option>
						<option value="0">实名</option>
					</select> 
					<span>&nbsp;</span>
				</div>
				<div class="xx_inputs">
					<label for="Name">留&nbsp;言&nbsp;人：</label> <input type="text" id="Name" name="Name"  placeholder="留言人" title="留言人"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="Sex">称&nbsp;&nbsp;&nbsp;&nbsp;呼：</label> 
					<select id="Sex">
						<option value="先生">先生</option>
						<option value="女士">女士</option>
					</select> 
					<span>&nbsp;</span>
				</div>
				<div class="xx_inputs">
					<label for="tel">联系电话：</label> <input type="text" id="Phone" name="Phone" placeholder="联系电话" title="联系电话"/> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="title">地&nbsp;&nbsp;&nbsp;&nbsp;址：</label> <input type="text" id="Address" name="Address" placeholder="地址" title="地址"/> <span>&nbsp;</span>
				</div>
				<div class="xx_inputs">
					<label for="con">留言内容：</label> <textarea type="text" id="Con" name="Con" placeholder="留言内容" title="留言内容"></textarea> <span>*</span>
				</div>
				<div class="xx_inputs">
					<label for="certicode">验&nbsp;证&nbsp;码：</label> <input name="certicode" id="certicode" title="请输入验证码" type="text" onfocus="esdonfocus();"/>&nbsp; 
											<img id="verifyImage" title="验证码" alt="验证码"/>
											<a href="javascript:esdonfocus();" title="播放验证码"><img id="audio" src="../etc/image/volume2.png" alt="播放验证码" title="播放验证码" /></a>
											<input type="hidden" id="voicepath" /> <span>*</span>
				</div>
				<div class="xx_btns">
					<button type="button" class="upd_pwd_btn" onclick="goSubmit()" title="提交">提交</button>
					<button type="button" class="clean_pwd_btn" onclick="qk()" title="重置">重置</button>
				</div>
			</div>
		</div>
	</div>
	<div id ="footer"></div>
</body>
</html>
