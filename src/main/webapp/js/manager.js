	var urls_content = document.getElementById("urls");
	var urlsTextArea = CodeMirror.fromTextArea(urls_content, {
		keyMap : "sublime",
		styleActiveLine : true,
		lineNumbers : true
	});
	var template_content = document.getElementById("template_content");
	var templateEditor = CodeMirror.fromTextArea(template_content, {
		mode : "text/html",
		keyMap : "sublime",
		matchTags : {
			bothTags : true
		},
		extraKeys : {
			"Ctrl-J" : "toMatchingTag",
			"Ctrl-M" : "autocomplete",
			"F11" : function(cm) {
				cm.setOption("fullScreen", !cm.getOption("fullScreen"));
			},
			"Esc" : function(cm) {
				if (cm.getOption("fullScreen"))
					cm.setOption("fullScreen", false);
			}
		},
		styleActiveLine : true,
		lineNumbers : true,
		autoCloseTags : true,
		autoCloseBrackets : true,
		matchBrackets : true,
		gutters : [ "CodeMirror-lint-markers" ]
	});

	var css_content = document.getElementById("css_content");
	var cssEditor = CodeMirror.fromTextArea(css_content, {
		mode : "text/css",
		keyMap : "sublime",
		matchTags : {
			bothTags : true
		},
		extraKeys : {
			"Ctrl-J" : "toMatchingTag",
			"Ctrl-M" : "autocomplete",
			"F11" : function(cm) {
				cm.setOption("fullScreen", !cm.getOption("fullScreen"));
			},
			"Esc" : function(cm) {
				if (cm.getOption("fullScreen"))
					cm.setOption("fullScreen", false);
			}
		},
		styleActiveLine : true,
		lineNumbers : true,
		autoCloseTags : true,
		autoCloseBrackets : true,
		matchBrackets : true,
		gutters : [ "CodeMirror-lint-markers" ],
	});

	var js_content = document.getElementById("js_content");
	var jsEditor = CodeMirror.fromTextArea(js_content, {
		mode : "text/javascript",
		keyMap : "sublime",
		matchTags : {
			bothTags : true
		},
		extraKeys : {
			"Ctrl-J" : "toMatchingTag",
			"Ctrl-M" : "autocomplete",
			"F11" : function(cm) {
				cm.setOption("fullScreen", !cm.getOption("fullScreen"));
			},
			"Esc" : function(cm) {
				if (cm.getOption("fullScreen"))
					cm.setOption("fullScreen", false);
			}
		},
		styleActiveLine : true,
		lineNumbers : true,
		autoCloseTags : true,
		autoCloseBrackets : true,
		matchBrackets : true,
		gutters : [ "CodeMirror-lint-markers" ],
	});
	/*******************************************************************************/
	createSite = function() {
		$.ajax({
			type : 'POST',
			url : 'createSite',
			dataType : 'json',
			data : {
				type : 0
			},
			success : function(data) {
				alert("回话：" + data);
			}
		});
	};

	insertFiles = function() {
		var fileType = $("#test").val();
		var dir = $("#dir").val();
		var siteName = $("#siteName").val();
		alert(fileType);
		$.ajax({
			type : 'POST',
			url : '/dbFiles/insertFiles',
			dataType : 'json',
			data : {
				fileType : fileType,
				url : dir,
				siteName : siteName
			},
			success : function(data) {
				alert("回话：" + data);
			}
		});
	};
	view1 = function() {
		var url = $("#url").val();
		var rules = getRules();
		var sleep = $("#sleep").val();
		var javaScriptEnabled = $("#javaScriptEnabled").combobox('getValue');
		var template = $("#templateName").val();
		if (url == "" || url.length == 0) {
			alert("请填写预览的域名！");
		} else {
			window.open('/iac/view?url=' + url + '&&rules=' + rules
					+ '&&sleep=' + sleep + '&&javaScriptEnabled='
					+ javaScriptEnabled + '&&template=' + template);

		}
	};
	countTimes = function() {

		$.ajax({
			url : root + '/countTimes',
			type : 'POST',
			sync : false,
			data : {

			},
			success : function(data) {
				$.messager.alert('提示', data.countTimes, 'info');
			},
			error : function() {
				$.messager.alert('发生错误', '定时任务设置失败!', 'error');
			}
		});
	}