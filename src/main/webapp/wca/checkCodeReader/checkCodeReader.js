
var checkCodeReader = {};
checkCodeReader.filesURL = "";//加载插件的路径  下方代码中自动获取
checkCodeReader.speakLoadSWF = "swf/soundmanager2.swf";//soundManager2Flash路径
checkCodeReader.speakOpenCloud = "mp3/openCloud.mp3";//提示按快捷键进入云平台
checkCodeReader.speakAjaxUrl = "http://voice.yunmd.net/ws/t2s";//无障碍云转换MP3路径

checkCodeReader.codeUrls = new Array();
checkCodeReader.index = 0;

checkCodeReader.loadfiles = function(){//加载插件
	var fileUrl = document.getElementById("checkCodeReader").getAttribute("src");
	if(fileUrl.indexOf("/")!=-1){
		checkCodeReader.filesURL = fileUrl.substring(0,fileUrl.lastIndexOf("/")+1);
	}else{
		checkCodeReader.filesURL = "";
	}
};
checkCodeReader.loadfiles();

var jq_ccr = $.noConflict(true);
checkCodeReader.speak = {};
checkCodeReader.speak.urls = {
		localSWF:checkCodeReader.filesURL + checkCodeReader.speakLoadSWF,
		openCloud:checkCodeReader.filesURL + checkCodeReader.speakOpenCloud
};

checkCodeReader.speak.init = function(){//soundManager2初始化
	soundManager.setup({
		useFlashBlock: false,
		url: checkCodeReader.speak.urls.localSWF, 
		debugMode: false,
		consoleOnly: false
	});
	soundManager.useFastPolling = false;
    soundManager.ontimeout(function(){  
    }); 
};

function readCheckCode(){
	var temp = jq_ccr("#voicepath").val();
	temp = temp.split(",");
	checkCodeReader.codeUrls = new Array();
	for(var i = 1; i< temp.length; i++){
		checkCodeReader.codeUrls.push(temp[0]+temp[i].split("?")[0]);
	}
	console.log(checkCodeReader.codeUrls);
	checkCodeReader.index = 0;
	soundManager.destroySound('checkCodeReader');
	codeReader();
	
};

function codeReader(){
	if(checkCodeReader.codeUrls.length>checkCodeReader.index){
		url = checkCodeReader.codeUrls[checkCodeReader.index];
		checkCodeReader.speak.speakOpenCloud=soundManager.createSound({
			id:'checkCodeReader',
			url:url,
			onfinish:function(){
				checkCodeReader.speak.speakOpenCloud.destruct();
				checkCodeReader.index++;
				codeReader()
			}
		});
		checkCodeReader.speak.speakOpenCloud.play();
	}else{
		checkCodeReader.index = 0;
		return ;
	}
	
}

jq_ccr(document).ready(function(){
	checkCodeReader.speak.init();
});
	