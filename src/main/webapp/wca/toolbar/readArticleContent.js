/*--
 Copyright (c) 2013 哈尔滨亿时代数码科技开发有限公司（www.hrbesd.com）. All rights reserved.
  
  HRBESD PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
--*/
/*
 * 朗读文章正文   正文朗读  快捷键 alt+shift+R
 * */

var readArticleContent = {};

readArticleContent.isOn = false;//正文朗读处理开关
readArticleContent.contentDom = ".body";  //正文节点标记  使用方式和jquery选择器相同  class用.开头  id用#开头

readArticleContent.readTrigger = function(){  //绑定快捷键
	if(iframe_change==1){  //保证document只绑定一次keydown事件
		jq_1_7_1(document).keydown(function(e){
			if (e.altKey && e.shiftKey) {
				if (e.keyCode == 82) {
					readArticleContent.read();
				}
			}
		});
	}
	jq_1_7_1(document.getElementById("iframe").contentWindow.document).keydown(function(e){
		if (e.altKey && e.shiftKey) {
			if (e.keyCode == 82) {
				readArticleContent.read();
			}
		}
	});
};

readArticleContent.read = function(){
	var readFlag = false;  //是否处于正文朗读标记   默认false 用来判断处理正文朗读结束时关闭连读用
	var domFirst = readArticleContent.contentDom+" ins:first";
	var domLast = readArticleContent.contentDom+" ins:last";
	jq_1_7_1("#toolbar_batchRead").click(function(){  
		readFlag = false;  //点击连读不属于正文朗读
	});
	jq_1_7_1(esd_tool_iframe).find(domFirst).focus();  //找到正文元素的第一个需要朗读的元素聚焦
	if(statusbatchRead != "off"){
		jq_1_7_1("#toolbar_batchRead").trigger("click");	//开启连读
	}
	readFlag = true;  //按快捷键进入正文朗读
	jq_1_7_1(esd_tool_iframe).find(domFirst).trigger("mouseover.pSpeak");  //触发连读功能
	jq_1_7_1(esd_tool_iframe).find(domLast).blur(function(){
		if(statusbatchRead == "off" && readFlag){//当连读是开 并且处于正文朗读功能时在读完最后一句话后才关闭连读
			jq_1_7_1("#toolbar_batchRead").trigger("click");	
			readFlag = false;
		}
	});
};




