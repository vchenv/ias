/*--
 Copyright (c) 2013 哈尔滨亿时代数码科技开发有限公司（www.hrbesd.com）. All rights reserved.
  
  HRBESD PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
--*/
/*
 * 处理辅助工具自身文本朗读
 * */

var topPageReader = {};

topPageReader.isOn = false;// 页面再处理开关

topPageReader.tpReader = function() {
	jq_1_7_1(window.document.body).find(".ESDAssetsTextCon").each(function() {
		jq_1_7_1(this).unbind("mouseover.pSpeak").unbind("mouseout.pSpeak");
		jq_1_7_1(this).unbind("focus.fSpeak").unbind("blur.fSpeak");
	});
	jq_1_7_1(window.document.body).find(".ESDAssetsTextCon").each(function() {
		if(this.tagName!="INPUT"){
			var psObj = jq_1_7_1(this);
			psObj.bind("mouseover.pSpeak", function() {
				pointSpeakTimeId = setTimeout(function() {
					topPageReader.callSM2(psObj[0]);
				}, 500);
				
			});
			psObj.bind("mouseout.pSpeak", function() {
				window.clearTimeout(pointSpeakTimeId);
			});
		}
	});
};

// 点读与键盘朗读调用sound
topPageReader.callSM2 = function(GuTarget) {
	var text = "";
	text = topPageReader.manageText(GuTarget);
	if (text.replace(/[ ]/g, "").length == 0) {
		return;
	}

	var de = base64.e64(text);
	jq_1_7_1.ajax({
		type : 'GET',
		url : speak.sound.url.ajaxUrl,
		dataType : 'jsonp',
		jsonp : "callback",
		data : {"b" : de, "s" : speak.speedVal},
		async : false,
		success : function(data) {
			if (speak.sound.mp3Object != null) {
				speak.sound.mp3Object.destruct();
			}
			speak.sound.mp3Object = soundManager.createSound({
				id : 'tool',
				url : data.u,
				onload : function(l) {
					if (l == 0) {
						if (speak.sound.mp3Object != null) {
							speak.sound.mp3Object.destruct();
						}
					}
				}
			});
			speak.sound.mp3Object.play();
		}
	});

};

topPageReader.manageText = function(obj){
	var text="";
	if(obj.tagName == "A"&&!(jq_1_7_1(obj).find("img").is("img"))&&!(jq_1_7_1(obj).find("span").is("span"))){
		if(obj.title!=null&&obj.title.length!=""){
			text = obj.title.replace(/<.+?>/g,'');
		}else {
			text = obj.innerHTML.replace(/<.+?>/g,'');
		}
		text = text;
	}else if(obj.tagName == "IMG"){
		if(obj.alt!=null&&obj.alt!=""&&obj.alt.replace(/\s/g, '') != "无意义图片"){
			text = obj.alt.replace(/\s/g, '');
			return text;
		}else{
			if(jq_1_7_1(obj).parents().is("a")){
				text = (jq_1_7_1(obj).parents('a').attr("title")!=undefined)? jq_1_7_1(obj).parents('a').attr("title") : "";
				text = text.replace(/\s/g, '');
				return text;
			}
			return "图片";
		}
	}else if(obj.tagName == "SPAN"){
		text = obj.innerHTML.replace(/<.+?>/g,'');
	}
	text = text.replace(/\|/g,'');
	text = text.replace(/\·/g,'');
	text = text.replace(/&lt;/g,'');
	text = text.replace(/&gt;/g,'');
	text = text.replace(/&nbsp;/g,'');
	text = text.replace(/\-/g,'');
	text = text.replace(/\→/g,'');
	text = text.replace(/\.\.\./g,'');
	return text;
};